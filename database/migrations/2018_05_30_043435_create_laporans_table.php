<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaporansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laporans', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('mitra_id');
            $table->unsignedInteger('angkot_id');
            $table->unsignedInteger('user_id');
            $table->dateTime('tanggal');
            $table->string('nopol');
            $table->longText('isi');
            $table->longText('balasan');
            $table->timestamps();

            $table->foreign('mitra_id')
            ->references('id')
            ->on('mitras')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('angkot_id')
            ->references('id')
            ->on('angkots')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laporans');
    }
}
