<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAngkotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('angkots', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('mitra_id');
            $table->string('nomor');
            $table->string('image');
            $table->longText('latlng')->nullable();
            $table->string('awal_latlng');
            $table->string('tujuan_latlng');
            $table->longText('rutes');
            $table->string('p_nomor')->nullable();
            $table->string('p_image')->nullable();
            $table->longText('p_latlng')->nullable();
            $table->string('p_awal_latlng')->nullable();
            $table->string('p_tujuan_latlng')->nullable();
            $table->longText('p_rutes')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();

            $table->foreign('mitra_id')
            ->references('id')
            ->on('mitras')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('angkots');
    }
}
