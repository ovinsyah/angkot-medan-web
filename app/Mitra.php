<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Mitra extends Authenticatable
{
    use Notifiable;
    protected $guard='mitra';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama', 'email', 'password','contact','image','alamat','deskripsi','status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

     public function angkot()
    {
        return $this->hasMany(Angkot::class);
    }

    public function laporan()
    {
        return $this->hasMany(Laporan::class);
    }
}
