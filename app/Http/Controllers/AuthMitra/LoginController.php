<?php

namespace App\Http\Controllers\AuthMitra;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class LoginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:mitra')->except(['logout','logout_mitra']);
    }

       public function showLoginForm()
    {
        return view('authMitra.login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|string',
        ]);
         $credential= [
            'email' => $request->email,
            'password' => $request->password,
        ];

       if (Auth::guard('mitra')->attempt($credential,$request->member)) {
           return redirect()->intended('/mitra/index') ;//suuces
       }
       return redirect()->back()->withInput($request->only('email','remember'))->withErrors(["email"=>"email tidak ada"]);//failed
    }

        public function logout_mitra()
    {
        Auth::guard('mitra')->logout();

        return redirect('/');
    }
}
