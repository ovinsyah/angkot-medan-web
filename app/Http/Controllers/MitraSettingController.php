<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mitra;
use App\Laporan;
use Auth;
use File;
use Illuminate\Support\Facades\Hash;
class MitraSettingController extends Controller
{
    public function profile()
    {
        $mitra=Auth::user();
    	return view('mitra.setting_profile',compact('mitra'));
    }
    public function editprofile()
    {
        $mitra=Auth::user();
    	return view('mitra.setting_edit_profile',compact('mitra'));
    }

    public function updateprofile(Request $request)
    {
        $mitra=Auth::user();
        $mitra->nama=$request->nama;
        $mitra->email=$request->email;
        $mitra->password=bcrypt($request->password);
        $mitra->contact=$request->contact;
        if ($request->hasFile('image')) {
            $image_path = public_path().'/images/mitra/'.$mitra->image;
            if(File::exists($image_path)){unlink($image_path);}

            $file=$request->file('image');
                $ext = $file->getClientOriginalExtension();
                $filenames=rand(100000000,999999999).".".$ext;
                $file->move('images/mitra',$filenames);
            $mitra->image=$filenames;
        }

        $mitra->alamat=$request->alamat;
        $mitra->deskripsi=$request->deskripsi;
        $mitra->save();

        return back();
    }
    public function account()
    {
        return view('mitra.setting_account');
    }
    public function changepassword()
    {
        return view('mitra.setting_change_password');
    }
    public function updatepassword(Request $request){
        $mitra=Auth::user();
        if (Hash::check($request->old_password,$mitra->password)) {
            $mitra->password=bcrypt($request->new_password);
            $mitra->save();
            return redirect('/mitra/setting/account')->with('success','berhasil merubah password');
        }else{
            return back()->with('error','gagal merubah password');//back()->with('status','error password lama salah');
        }
    }
    public function replayreport(Request $request,$id)
    {
        $laporan=Laporan::find($id);
        $laporan->balasan = $request->balasan;
        $laporan->save();
        return back();
    }
}
