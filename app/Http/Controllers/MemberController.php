<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use File;
use Hash;
	
class MemberController extends Controller
{
    public function index()
    {
    	return view('member.index');
    }
    public function password()
    {
    	return view('member.change_password');
    }

    public function update_profile(Request $request)
    {
    	$user=Auth::user();
    	$user->name=$request->name;

    	if ($request->hasFile('image')) {
    	    $image_path = public_path().'/images/user/'.$user->image;
    	    if(File::exists($image_path && $user->image!="profile.png")){unlink($image_path);}

    	    $file=$request->file('image');
    	        $ext = $file->getClientOriginalExtension();
    	        $filenames=rand(100000000,999999999).".".$ext;
    	        $file->move('images/user',$filenames);
    	    $user->image=$filenames;
    	}
    	$user->save();
    	return back();
    }

    public function update_password(Request $request)
    {
        $user=Auth::user();
        if (Hash::check($request->old,$user->password)) {
            $user->password=bcrypt($request->new);
            $user->save();
            return redirect('/user')->with(['success' => 'berhasil merubah password']);
        }else{
            return redirect('/user/change-password')->with(['error' => 'gagal merubah password']);
        }
    }
}
