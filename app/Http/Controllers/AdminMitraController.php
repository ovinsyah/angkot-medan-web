<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mitra;
use File;
use Illuminate\Support\Facades\Mail;
use App\Mail\AdminMail;
use Illuminate\Support\Facades\Validator;

class AdminMitraController extends Controller
{
    public function index()
    {
        $mitras=Mitra::where('status',1)->get();
    	return view('admin.mitra',compact('mitras'));
    }
    public function add()
    {

    	return view('admin.add_mitra');
    }
    public function detail($id)
    {
        $mitra=Mitra::find($id);
        return view('admin.detail_mitra',compact('mitra'));
    }
    public function request()
    {  
        $mitras=Mitra::where('status',0)->get();
        return view('admin.request_mitra',compact('mitras'));
    }
    public function acceptrequest($id)
    {
        $mitra=Mitra::find($id);
    	return view('admin.accept_request_mitra',compact('mitra'));
    }
    public function requestdetail()
    {
        return view('admin.request_detail_mitra');
    }

     public function create(Request $request)
    {
        //return dd($request->all(),$request->file('image'));
        Validator::make($request->all(), [
            'nama' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:mitras',
        ])->validate();

        $mitra=new Mitra();
        $mitra->nama=$request->nama;
        $mitra->email=$request->email;
        $mitra->password=bcrypt($request->password);
        $mitra->contact=$request->contact;

        $file=$request->file('image');
            $ext = $file->getClientOriginalExtension();
            $filenames=rand(100000000,999999999).".".$ext;
            $file->move('images/mitra',$filenames);
        $mitra->image=$filenames;

        $mitra->alamat=$request->alamat;
        $mitra->deskripsi=$request->deskripsi;
        if($mitra->save()){
           $objDemo = new \stdClass();
            $objDemo->usaha = $request->nama;
            $objDemo->email = $request->email;
            $objDemo->password = $request->password;
     
        Mail::to($request->email)->send(new AdminMail($objDemo));
        }

        return redirect('admin/mitra');
    }

        public function update(Request $request,$id)
    {
        //return dd($request->all(),$request->file('image'));
        $mitra=Mitra::find($id);
        $mitra->nama=$request->nama;
        $mitra->email=$request->email;
        $mitra->password=bcrypt($request->password);
        $mitra->contact=$request->contact;
        $mitra->status=1;
        if ($request->hasFile('image')) {
            $image_path = public_path().'/images/mitra/'.$mitra->image;
            if(File::exists($image_path)){unlink($image_path);}

            $file=$request->file('image');
                $ext = $file->getClientOriginalExtension();
                $filenames=rand(100000000,999999999).".".$ext;
                $file->move('images/mitra',$filenames);
            $mitra->image=$filenames;
        }

        $mitra->alamat=$request->alamat;
        $mitra->deskripsi=$request->deskripsi;
        if($mitra->save()){
           $objDemo = new \stdClass();
            $objDemo->usaha = 'Usaha';
            $objDemo->email = $request->email;
            $objDemo->password = $request->password;
     
        Mail::to($request->email)->send(new AdminMail($objDemo));
        }

        return redirect('admin/request/mitra');
    }

    public function delete($id)
    {
       $mitra=Mitra::find($id);
       $image_path = public_path().'/images/mitra/'.$mitra->image;
        if(File::exists($image_path)){unlink($image_path);}

        foreach ($mitra->angkot as $angkot) {
               $image_path = public_path().'/images/angkot/'.$angkot->image;
                if(File::exists($image_path)){unlink($image_path);}
               $angkot->delete();
        }

       $mitra->delete();
       return response()->json(["data"=>"terhapus ".$id],200);
    }
}
