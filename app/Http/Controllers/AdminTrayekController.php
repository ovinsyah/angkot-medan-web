<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Angkot;
use File;
class AdminTrayekController extends Controller
{
    public function index()
    {
        $angkots=Angkot::where('status',1)->paginate(20);
    	return view('admin.trayek_angkot',compact('angkots'));
    }
    public function pending()
    {
        $angkots=Angkot::where('status','!=',1)->paginate(20);
    	return view('admin.pending_trayek_angkot',compact('angkots'));
    }
    public function detail($id)
    {
        $angkot=Angkot::find($id);
    	return view('admin.detail_trayek_angkot',compact('angkot'));
    }
    public function pendingdetail()
    {
        return view('admin.detail_trayek_angkot');
    }

    public function approve_trayek($id)
    {
       $angkot=Angkot::find($id);
       if ($angkot->status==3) {//hapus trayek diterima
            $image_path = public_path().'/images/angkot/'.$angkot->image;
            if(File::exists($image_path)){unlink($image_path);}
            $angkot->delete();
       }elseif ($angkot->status==2) {// edit trayek diterima
            $angkot->status=1;
            if ($angkot->p_image) {
               $image_path = public_path().'/images/angkot/'.$angkot->image;
                if(File::exists($image_path)){
                    unlink($image_path);
                }//hapus file lama

                //update value 
              $angkot->image=$angkot->p_image;
            }
              $angkot->nomor=($angkot->p_nomor)?$angkot->p_nomor:$angkot->nomor;
              $angkot->latlng=($angkot->p_latlng)?$angkot->p_latlng:$angkot->latlng;
              $angkot->awal_latlng=($angkot->p_awal_latlng)?$angkot->p_awal_latlng:$angkot->awal_latlng;
              $angkot->tujuan_latlng=($angkot->p_tujuan_latlng)?$angkot->p_tujuan_latlng:$angkot->tujuan_latlng;
              $angkot->rutes=($angkot->p_rutes)?$angkot->p_rutes:$angkot->rutes;

            //clear tmp val
            $angkot->p_nomor=null;
            $angkot->p_image=null;
            $angkot->p_latlng=null;
            $angkot->p_awal_latlng=null;
            $angkot->p_tujuan_latlng=null;
            $angkot->p_rutes=null;

            $angkot->save();
       }elseif ($angkot->status==0) {
            $angkot->status=1;
            $angkot->save();
       }

       return back();
    }

    public function delete_trayek($id)
    {
       $angkot=Angkot::find($id);
         if ($angkot->status==3) {
              $angkot->status=6;//hapus trayek ditolak
         }elseif ($angkot->status==2) {
              $angkot->status=4;//edit trayek di tolak
         }elseif ($angkot->status==1) {//add trayek di tolak
              $image_path = public_path().'/images/angkot/'.$angkot->image;
              if(File::exists($image_path)){unlink($image_path);}
              $angkot->delete();

              return back();
        }elseif ($angkot->status==0) {//hapus trayek di aktif
              $image_path = public_path().'/images/angkot/'.$angkot->image;
              if(File::exists($image_path)){unlink($image_path);}
              $angkot->delete();

              return back();
        }
      $angkot->save();

       return back();
    }
}
