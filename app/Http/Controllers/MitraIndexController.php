<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Laporan;
use Auth;
class MitraIndexController extends Controller
{
    public function index()
    {
    	return view('mitra.index');
    }
    public function report()
    {
    	$laporans=Laporan::where('mitra_id',Auth::user()->id)->get();
    	return view('mitra.report',compact('laporans'));
    }
    public function detailreport($id)
    {
    	$laporan=Laporan::find($id);
    	return view('mitra.detail_report',compact('laporan'));
    }
}
