<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use App\Laporan;
use App\Angkot;
use App\User;
use File;
class AdminIndexController extends Controller
{
 
    public function index()
    {
    	return view('admin.index');
    }
    public function member()
    {   
        $members = User::all();
        return view('admin.user',compact('members'));
    }
    public function contact()
    {
        $contacts=Contact::all();
    	return view('admin.contact',compact('contacts'));
    }
    public function report()
    {   
        $laporans=Laporan::all();
        return view('admin.report',compact('laporans'));
    }
    public function detailreport($id)
    {
        $laporan=Laporan::find($id);
        return view('admin.detail_report',compact('laporan'));
    }
        public function deletereport($id)
    {
        $laporan=Laporan::find($id);
        $laporan->delete();
        return back();
    }

    public function create_contact(Request $request)
    {
        $contact=new Contact();
        $contact->judul=$request->judul;
        $contact->deskripsi=$request->deskripsi;

         $file=$request->file('icon');
            $ext = $file->getClientOriginalExtension();
            $filenames=rand(100000000,999999999).".".$ext;
            $file->move('images/contact',$filenames);
        $contact->icon=$filenames;

        $contact->save();
        return redirect('admin/contact');
    }

       public function update_contact(Request $request,$id)
    {
        $contact=Contact::find($id);
        $contact->judul=$request->judul;
        $contact->deskripsi=$request->deskripsi;

         if ($request->hasFile('icon')) {
            $image_path = public_path().'/images/contact/'.$contact->icon;
            if(File::exists($image_path)){unlink($image_path);}

                 $file=$request->file('icon');
                    $ext = $file->getClientOriginalExtension();
                    $filenames=rand(100000000,999999999).".".$ext;
                    $file->move('images/contact',$filenames);
                $contact->icon=$filenames;
        }
        $contact->save();
        return redirect('admin/contact');
    }

    public function delete_contact($id)
    {
       $contact=Contact::find($id);
       $image_path = public_path().'/images/contact/'.$contact->icon;
        if(File::exists($image_path)){unlink($image_path);}

       $contact->delete();
       return back();
    }

    public function delete_member($id)
    {
       $user=User::find($id);
       $image_path = public_path().'/images/user/'.$user->image;
        if(File::exists($image_path && $user->image!="profile.png")){unlink($image_path);}

       $user->delete();
       return back();
    }

}
