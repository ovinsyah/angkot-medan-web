<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Angkot;
use File;
use Auth;
class MitraTrayekController extends Controller
{
    public function index()
    {
        $angkots=Angkot::where('status',1)->where('mitra_id',Auth::guard('mitra')->user()->id)->get();
    	return view('mitra.trayek_angkot',compact('angkots'));
    }
    public function add()
    {
    	return view('mitra.add_trayek_angkot');
    }
    public function detail($id)
    {
      $angkot=Angkot::find($id);
    	return view('mitra.detail_trayek_angkot',compact('angkot'));
    }
    public function edit($id)
    {
        $angkot=Angkot::find($id);
        return view('mitra.edit_trayek_angkot',compact('angkot'));
    }
    public function pending()
    {
        $angkots=Angkot::where('status','!=',1)->where('mitra_id',Auth::guard('mitra')->user()->id)->get();
        return view('mitra.pending_trayek_angkot',compact('angkots'));
    }
    public function editpending()
    {
        return view('mitra.edit_pending_trayek_angkot');
    }
    public function pendingdetail()
    {
    	return view('mitra.detail_trayek_angkot');
    }
    public function create_trayek(Request $request)
    {
       $rutes=explode(',',$request->rute);
       $filter=array();
       foreach ($rutes as $key ) {
           if(strpos($key, 'Jl.') !== false || strpos($key, 'Jalan') !== false){
            if (strpos($key, '/') !== false) {
               $tmp=explode('/',$key);
               $filter[] =str_replace("Jalan ","Jl. ",$tmp[0]);
               $filter[] =str_replace("Jalan ","Jl. ",$tmp[1]);
            }else{
              $filter[] =str_replace("Jalan ","Jl. ",$key);
            }
           }  
       }
       // return response()->json(['all'=>$request->all(),"rutes"=>$filter],200);
       $angkot=new Angkot();
       $angkot->mitra_id=$request->mitra;
       $angkot->nomor=$request->nomor;
       $angkot->latlng=$request->latlng;
       $angkot->awal_latlng=$request->awal_latlng;
       $angkot->tujuan_latlng=$request->tujuan_latlng;
       $angkot->rutes=implode(',',array_unique(explode(',',$request->awal.','.implode(',',$filter).','.$request->tujuan)));

        $file=$request->file('image');
            $ext = $file->getClientOriginalExtension();
            $filenames=rand(100000000,999999999).".".$ext;
            $file->move('images/angkot',$filenames);
        $angkot->image=$filenames;

        if ($angkot->save()) {
                return response()->json(['success'=>[$angkot,$rutes]],200);
        }
        return response()->json(['failed'=>$request->all()],200);
    }

    public function update_trayek($id,Request $request)
    {
       $rutes=explode(',',$request->rute);
       $filter=array();
       foreach ($rutes as $key ) {
           if(strpos($key, 'Jl.') !== false || strpos($key, 'Jalan') !== false){
            if (strpos($key, '/') !== false) {
               $tmp=explode('/',$key);
               $filter[] =str_replace("Jalan ","Jl. ",$tmp[0]);
               $filter[] =str_replace("Jalan ","Jl. ",$tmp[1]);
            }else{
              $filter[] =str_replace("Jalan ","Jl. ",$key);
            }
           }  
       }
       //return response()->json(['all'=>$request->all(),"rutes"=>$filter],200);
       $angkot=Angkot::find($id);
       $angkot->p_nomor=$request->nomor;
       $angkot->status=2;
       $angkot->p_latlng=$request->latlng;
       $angkot->p_awal_latlng=$request->awal_latlng;
       $angkot->p_tujuan_latlng=$request->tujuan_latlng;
       $angkot->p_rutes=implode(',',array_unique(explode(',',$request->awal.','.implode(',',$filter).','.$request->tujuan)));

       if ($request->hasFile('image')) {
          $file=$request->file('image');
              $ext = $file->getClientOriginalExtension();
              $filenames=rand(100000000,999999999).".".$ext;
                 $image_path=public_path().'/images/angkot/'.$angkot->image;
                 if (File::exists($image_path)){ unlink($image_path);}
              $file->move('images/angkot',$filenames);
              $angkot->p_image=$filenames;
       }

        if ($angkot->save()) {
                return response()->json(['success'=>[$angkot,$rutes]],200);
        }
        return response()->json(['failed'=>$request->all()],200);
    }

    public function delete_trayek($id)
    {
       $angkot=Angkot::find($id);
       $angkot->status=3;
       $angkot->save();
      /* $image_path = public_path().'/images/angkot/'.$angkot->image;
        if(File::exists($image_path)){unlink($image_path);}

       $angkot->delete();*/
       return back();
    }

     public function delete_trayek_pending($id)
    {
       $angkot=Angkot::find($id);
       if ($angkot->status==2) {
        if($angkot->p_image){
             $image_path = public_path().'/images/angkot/'.$angkot->p_image;
             if(File::exists($image_path)){unlink($image_path);}
        }

            //clear tmp val
            $angkot->p_nomor=null;
            $angkot->p_image=null;
            $angkot->p_latlng=null;
            $angkot->p_awal_latlng=null;
            $angkot->p_tujuan_latlng=null;
            $angkot->p_rutes=null;

            $angkot->status=1;
       }else{
            $angkot->status=1;
       }

       $angkot->save();
       return back();
    }
}
