<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mitra;
use App\Angkot;
use App\Laporan;
use App\Contact;
use Illuminate\Support\Facades\Validator;

class UserIndexController extends Controller
{
    public function index()
    {
    	return view('user/index');
    }
    public function search($awal,$tujuan)
    {
        $angkots=Angkot::where('status',1)->get();
        $result=array();
        foreach ($angkots as $angkot) {
            $array= explode(",",$angkot->rutes);
            if (in_array($awal,$array) && in_array($tujuan,$array)) {
                $result[]=$angkot;
            }
        }
        //return dd($result);
    	return view('user/search_trayek',compact('awal','tujuan','result'));
    }
    public function detail($id)
    {
        $angkot=Angkot::find($id);
        $laporans = Laporan::where('angkot_id',$id) ->orderBy('created_at', 'desc')->get();
    	return view('user/detail_trayek',compact('angkot','laporans'));
    }
    public function report($id)
    {
         $angkot=Angkot::find($id);
        return view('user/report',compact('angkot'));
    }

      public function create_report($id,Request $request)
    {
       // return dd($request->all());
         $laporan=new Laporan();
         $laporan->mitra_id=Angkot::find($id)->mitra->id;
         $laporan->angkot_id=$id;
         $laporan->user_id=$request->user_id;
         $laporan->tanggal=$request->tanggal;
         $laporan->nopol=$request->nopol;
         $laporan->isi=$request->isi;
         $laporan->save();
        return redirect('/detail/trayek/'.$id);
    }

    public function api_detail($id)
    {
        $angkot=Angkot::find($id);
        return response()->json(["angkot"=>$angkot],200);
    }

    public function angkot()
    {
        $angkots=Angkot::where('status',1)->get();
    	return view('user/angkot',compact('angkots'));
    }
    public function searchangkot()
    {   
        $id=request('angkot');
        $angkots=Angkot::where('status',1)->where('nomor',"LIKE","%".$id."%")->get();
        return view('user/search_angkot',compact('angkots','id'));
    }
    public function contact()
    {
        $contacts=Contact::all();
    	return view('user/contact',compact('contacts'));
    }
    public function mitra()
    {
    	return view('user/join-mitra');
    }

    public function create(Request $request)
    {
        //return dd($request->all(),$request->file('image'));
        Validator::make($request->all(), [
            'nama' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:mitras',
        ])->validate();
        $mitra=new Mitra();
        $mitra->nama=$request->nama;
        $mitra->email=$request->email;
        $mitra->password=bcrypt('12345');
        $mitra->contact=$request->contact;
        $mitra->status=0;

        $file=$request->file('image');
            $ext = $file->getClientOriginalExtension();
            $filenames=rand(100000000,999999999).".".$ext;
            $file->move('images/mitra',$filenames);
        $mitra->image=$filenames;

        $file2=$request->file('bukti');
            $ext2 = $file2->getClientOriginalExtension();
            $filenames2=rand(100000000,999999999).".".$ext;
            $file2->move('images/mitra',$filenames2);
        $mitra->bukti=$filenames2;

        $mitra->alamat=$request->alamat;
        $mitra->deskripsi=$request->deskripsi;
        $mitra->save();

        return redirect('join-mitra')->with('success','1');
    }

    public function sendmail()
    {
        return view ('user.send_mail');
    }

}
