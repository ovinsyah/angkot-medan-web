<?php

namespace App\Http\Controllers;
use App\Mitra;
use App\Laporan;
use Illuminate\Http\Request;

class CobaController extends Controller
{
    public function test()
    {
    	$lpr = Laporan::All();
    	$mtr = Mitra::All();
    	return view('user.test', compact('mtr', 'lpr'));

    }
}
