<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Angkot extends Model
{
  
     public function mitra()
    {
        return $this->belongsTo(Mitra::class);
    }

    public function laporan()
    {
        return $this->hasMany(Laporan::class);
    }


}
