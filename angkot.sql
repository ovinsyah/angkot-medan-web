-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 11 Agu 2018 pada 06.15
-- Versi Server: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `angkot`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@mail.com', '$2y$10$717WnLoIBj2Vx/2NaXz6P..MJ3ITZl6h4ydz.w4SBEQAmL1SZMzf.', 'FChxjbwRAccJU2I6UQ13it8m6PcegH1sWI5jxS5baGboIutGuOuDD3KE81xA', '2018-07-11 20:58:01', '2018-07-11 20:58:01'),
(4, 'admin2', 'admin2@mail.com', '$2y$10$iES7rUDTYp59hkKr6rbD8O5iBNk.YKrF891Am/EXJfHTU5S8fd3F2', 'hMuU8SQVUGRVSmKz2t4zOp9yIwL7G2JiKJH19XvE8O809G0j12mefbjom7mY', '2018-07-11 21:26:29', '2018-07-11 21:26:29'),
(5, 'admin', 'admin3@mail.com', '$2y$10$RlKDaMUEGhsjlo.8EeHgA.OjLStRSAwLfuq6IDW8l5mADw3DoDzN2', NULL, '2018-07-11 21:48:12', '2018-07-11 21:48:12'),
(6, '4', 'admin4@mail.com', '$2y$10$Q/38kMsxEcpJrUt0f/fal.rrtN9xW.Og/VGxhazrtr75J4OlSprRe', 'Dw0RKSimTa2eo8mV3k9zSg4us1ro67otbQdRSVay6hOWWXYHen41llRPZGHN', '2018-07-11 23:16:21', '2018-07-11 23:16:21');

-- --------------------------------------------------------

--
-- Struktur dari tabel `angkots`
--

CREATE TABLE `angkots` (
  `id` int(10) UNSIGNED NOT NULL,
  `mitra_id` int(10) UNSIGNED NOT NULL,
  `nomor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latlng` longtext COLLATE utf8mb4_unicode_ci,
  `awal_latlng` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tujuan_latlng` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rutes` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `p_nomor` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_latlng` longtext COLLATE utf8mb4_unicode_ci,
  `p_awal_latlng` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_tujuan_latlng` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_rutes` longtext COLLATE utf8mb4_unicode_ci,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `angkots`
--

INSERT INTO `angkots` (`id`, `mitra_id`, `nomor`, `image`, `latlng`, `awal_latlng`, `tujuan_latlng`, `rutes`, `p_nomor`, `p_image`, `p_latlng`, `p_awal_latlng`, `p_tujuan_latlng`, `p_rutes`, `status`, `created_at`, `updated_at`) VALUES
(3, 7, '2', '704181630.PNG', '3.5882738276848625|98.6905870531989', '3.5883887,98.68890729999998', '3.5878302,98.68912399999999', 'Jl. Dr. Fl. Tobing/Jl Bintang,Jl. Talaud,Jl. M.H Thamrin,Jl. Merbabu,Jl. Dr. Fl. Tobing,Jl Bintang', NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-07-26 00:37:56', '2018-07-26 00:44:29'),
(4, 7, '90', '879580043.jpg', '3.5910043|98.68961620000005,3.5901560311799687|98.67463119541208,3.5873849|98.70498789999999,3.5979372|98.70639499999993', '3.593710799999999,98.69097820000002', '3.5938502,98.69148949999999', 'Jl. Malaka,Jl. M.H Thamrin,Jl. Asia,Jl. Sutomo,Jl. Pandu,Jl. Pemuda,Jl. Jenderal Ahmad Yani,Jl. Pengadilan,Jl. Kapten Maulana Lubis,Jalan Walkot,Jl. Raden Saleh,Jl. Balaikota,Jl. Bukit Barisan,Jl. Kereta Api,Jl. M. T. Haryono,Jl. Asia Raya,Jl. Asia Indah,Jl. Arief Rahman Hakim,Jl. Aksara,Jl. Prof. HM. Yamin Sh,Jl. Madong Lubis', NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-08-10 06:22:56', '2018-08-10 06:24:42'),
(5, 7, '11', '611806637.jpg', '3.593526199999999|98.69030989999999,3.602032932052287|98.68806973991286,3.5959599|98.67871459999992', '3.5893651,98.70217260000004', '3.5893332,98.70184369999993', 'Pandau Hulu II,Jl. Sebarau,Jl. Wahidin,Jl. Bambu Runcing,Jl. Pahlawan,Jl. Prof. HM. Yamin Sh,Jl. Madong Lubis,Jl. Malaka,Jl. G.B. Joshua,Jl. Purwo,Jl. Dorowati,Jl. Pelita I,Jl. HM. Said,Jl. Bambu,Jl. Gaharu,Jl. Jawa,Jl. Irian Barat,Jl. M. T. Haryono,Jl. M.H Thamrin,Jl. Jurung', NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-08-10 06:24:16', '2018-08-10 06:24:38'),
(6, 7, '12', '375065781.jpg', NULL, '3.5877246,98.69086479999999', '3.5877799,98.69084229999999', 'Jalan M.H Thamrin,Jl. Wahidin,Jl. Merbabu,Jl. Dr. Fl. Tobing,Jl Bintang,Jl. M. T. Haryono,Jl. M.H Thamrin,Jl. Thamrin', NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-08-10 06:25:40', '2018-08-10 06:25:54');

-- --------------------------------------------------------

--
-- Struktur dari tabel `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `judul` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `laporans`
--

CREATE TABLE `laporans` (
  `id` int(10) UNSIGNED NOT NULL,
  `mitra_id` int(10) UNSIGNED NOT NULL,
  `angkot_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `tanggal` datetime NOT NULL,
  `nopol` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `balasan` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `laporans`
--

INSERT INTO `laporans` (`id`, `mitra_id`, `angkot_id`, `user_id`, `tanggal`, `nopol`, `isi`, `balasan`, `created_at`, `updated_at`) VALUES
(1, 7, 3, 3, '2018-07-27 00:00:00', 'bk 19997 io', 'Supir suka ugal-ugalan dijalan raya', 'gini om bla blafwfwfw', '2018-07-26 00:47:31', '2018-07-26 01:22:12'),
(2, 7, 3, 4, '2018-07-28 14:49:00', 'bk 19997 io', 'Supir tidak sopan', NULL, '2018-07-26 00:49:41', '2018-07-26 00:49:41');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_05_27_051330_create_mitras_table', 1),
(4, '2018_05_28_044022_create_contacts_table', 1),
(7, '2018_07_12_030312_create_admins_table', 2),
(8, '2018_05_30_043435_create_laporans_table', 3),
(10, '2018_05_29_045330_create_angkots_table', 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `mitras`
--

CREATE TABLE `mitras` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bukti` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `deskripsi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `mitras`
--

INSERT INTO `mitras` (`id`, `nama`, `email`, `password`, `contact`, `image`, `bukti`, `alamat`, `status`, `deskripsi`, `remember_token`, `created_at`, `updated_at`) VALUES
(7, 'PT Mars', 'mars@mail.com', '$2y$10$5v7wrtB9fCmUnlVG2roE6u8YxAMfZ4.zO0aAQDGRr2ekPHWUoWrJO', '0812123123123', '949111825.png', NULL, 'Jl. Denai', 1, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'c59OWlomByGHQ15mStwErQJoiS7gkdIo3NMCpjqbE2kCfGbNYif14AlIIXou', '2018-07-23 07:34:31', '2018-07-23 07:34:31'),
(8, 'PT Mujur Timber', 'mujur@mail.com', '$2y$10$BlCKtIzS/U1qHTHpXsGdAuPTtIypqGzqxYUHVA/Xg2lBUIuC24/d.', '081212312312', '321621758.png', NULL, 'Jl. Mandala', 1, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', NULL, '2018-07-23 07:36:05', '2018-07-23 07:36:05'),
(11, 'Bangun', 'bangunmanullang20@gmail.com', '$2y$10$uBludYP5ZnO4AGzQb7fNhexfkrUN3epYUV6Ih82iLaS8ofiJKCC0i', '0812123123123', '895288229.PNG', NULL, 'dvsd', 1, 'sdvdvds', NULL, '2018-07-26 05:33:07', '2018-07-26 05:33:07'),
(12, 'coba edit', 'oviensyahalbayhaqy@gmail.com', '$2y$10$WuBPovbsn78rezBucrZOsuaCkILcif2KYHp38c.r9oUMcBGRjWR3y', '0812123123123', '770730801.jpg', NULL, 'wdwdw', 1, 'wdwdw', NULL, '2018-08-03 07:14:46', '2018-08-03 07:14:46'),
(14, '2222222', 'admin2222222@mail.com', '$2y$10$u7.s3XxSk/rs1jaCMDGYrOchxVndBAoLBfxBvAjGkce.ImXAM9CNi', '222222222', '970108110.jpg', '168347890.jpg', '2222222222', 0, '222222222222', NULL, '2018-08-10 21:11:30', '2018-08-10 21:11:30');

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `image`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 'Rizky Febian (user1)', '910109324.jpg', 'user1@mail.com', '$2y$10$oip8HZe9JygN0JJo8V/fSOug3/BnOnSsVoblxWiO0MqjZVok.KmmO', 'g4954MUnI8TRKyo4qWQ8DVrBbtWMwbWXNNZxqPworXmj0IzsUmlp6CxF1XTD', '2018-07-23 08:19:13', '2018-07-23 08:23:50'),
(4, 'Ucok User3', NULL, 'user3@mail.com', '$2y$10$3MBiyPqnJD5FBeKkBHwJrOJoZ1S.gA3zDtRJZcIe8KdcSBxSNDxpK', 's0S6viXF2xCMfwMTTmTUFvfFol735bnOxNWd00wq4EJzthNdz2ZtFrwXa3bQ', '2018-07-26 00:47:54', '2018-07-26 00:47:54');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `angkots`
--
ALTER TABLE `angkots`
  ADD PRIMARY KEY (`id`),
  ADD KEY `angkots_mitra_id_foreign` (`mitra_id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laporans`
--
ALTER TABLE `laporans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `laporans_mitra_id_foreign` (`mitra_id`),
  ADD KEY `laporans_angkot_id_foreign` (`angkot_id`),
  ADD KEY `laporans_user_id_foreign` (`user_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mitras`
--
ALTER TABLE `mitras`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `angkots`
--
ALTER TABLE `angkots`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `laporans`
--
ALTER TABLE `laporans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `mitras`
--
ALTER TABLE `mitras`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `angkots`
--
ALTER TABLE `angkots`
  ADD CONSTRAINT `angkots_mitra_id_foreign` FOREIGN KEY (`mitra_id`) REFERENCES `mitras` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `laporans`
--
ALTER TABLE `laporans`
  ADD CONSTRAINT `laporans_angkot_id_foreign` FOREIGN KEY (`angkot_id`) REFERENCES `angkots` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `laporans_mitra_id_foreign` FOREIGN KEY (`mitra_id`) REFERENCES `mitras` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `laporans_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
