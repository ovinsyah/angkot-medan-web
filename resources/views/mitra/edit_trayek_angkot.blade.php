@extends('master.master_mitra')
@section('content')
<div class="content-admin" style="height: 80%">
	<div class="row ml-0 mr-0 mb-3 title-page-admin">
		<div class="col p-0">
			<div class="">Edit Trayek Angkot</div>
		</div>
		<div class="col p-0 text-right">
		<button class="btn btn-app" id="confirm" onclick="kirim()">kirim</button>
		</div>
	</div>
	<div class="row m-0 mb-5">
		<div class="col p-0" style="max-width: 15rem">
			<span class="text-bold font-16 pt-2">Nomor Angkot</span>
		</div>
		<div class="col p-0">
			<input type="number" id="nomor" class="form-control" style="width: 7rem" value="{{$angkot->nomor}}">
			<input type="hidden" id="mitra" class="form-control" style="width: 7rem" value="{{$angkot->mitra->id}}">
		</div>
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 15rem">
			<span class="text-bold font-16 pt-2">Gambar Angkot</span>
		</div>
		<div class="col p-0">
			<div class="image-view-upload d-inline-block">
				<input name="image" type="file" accept="image/*" onchange="uploadimage(event)" id="imginput" class="hidden" />
				<img id="imgview" src="{{asset('images/angkot')}}/{{$angkot->image}}" class="image-upload"/><br>
				<label class="mt-2 btn-change text-center" for="imginput">
					<span class="btn btn-upload">Pilih Gambar</span>
				</label>
			</div>
		</div>
	</div>
	<span class="text-bold font-16 pt-2">Jalur Angkot</span>
	<div class="row m-0" style="height: 70%">
		<div class="col p-0" id="map"></div>

		<div class="col pl-4 pr-3" id="panel">
			<p>Total Distance: <span id="total"></span></p>
		</div>
	</div>
</div>
<script>

	$('#adm-trayek').addClass('active');
	$('#adm-trayek').removeClass('collapsed');
    $('#pageSubmenu').attr( "aria-expanded", "true" );
	$('#pageSubmenu').addClass('in');
	$('#pageSubmenu').addClass('show');
	$('#adm-menu').attr( "aria-expanded", "true" );
	$('#adm-tr-angkot').addClass('active');
	
	var steps;
	var tmp1 =[]
	var z =[]
	var listjalan=[]
	var file;
	var awal;
	var tujuan;
	var awal_latlng;
	var tujuan_latlng;
	var dir_dis;
	var directionsService;
	var directionsDisplay;
	wpoin=[];

	$("#imginput").on("change",function () {
		file=this.files[0];
	});

		var api_latlng='{{$angkot->latlng}}'.split(',');
	function initMap() {
		console.log("satu",api_latlng);

		var api_awal='{{$angkot->awal_latlng}}';
		var api_tujuan='{{$angkot->tujuan_latlng}}';

		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 4,
          center: {lat: 3.5813667411452568, lng: 98.69381740856034}  // Australia.
      	});

		var directionsService = new google.maps.DirectionsService;
		var directionsDisplay = new google.maps.DirectionsRenderer({
			draggable: true,
			map: map,
			panel: document.getElementById('panel')
		});

		directionsDisplay.addListener('directions_changed', function() {
			var way=(directionsDisplay.directions.request.waypoints)?directionsDisplay.directions.request.waypoints: [];
			console.log(directionsDisplay,way,'ddis');
			computeTotalDistance(directionsDisplay.getDirections());
			//get latlang
			var route = directionsDisplay.getDirections().routes[0];
			var points = route.overview_path;
			tmp1=[];
			for (var i = 0; i < way.length; i++) {
				if (way[i].location.location) {
					tmp1.push(way[i].location.location.lat()+'|'+way[i].location.location.lng());
				}else{
					tmp1.push(way[i].location.lat()+'|'+way[i].location.lng());
				}
		    }
		    console.log(tmp1,"tmp1")


		});

		displayRoute(new google.maps.LatLng(parseFloat(api_awal.split(',')[0]),parseFloat(api_awal.split(',')[1])),new google.maps.LatLng(parseFloat(api_tujuan.split(',')[0]),parseFloat(api_tujuan.split(',')[1])), directionsService,directionsDisplay);
	}

	function displayRoute(origin, destination, service, display) {
		if(api_latlng>1){
			$.each(api_latlng,function (i,v) {
				var cordinat=new google.maps.LatLng(parseFloat(v.split('|')[0]),parseFloat(v.split('|')[1]))
				console.log(cordinat,"cordinat")
				wpoin.push({"location":cordinat,"stopover":false})
			})		
		}
			service.route({
			origin: origin,
			destination: destination,
			waypoints:wpoin,
			travelMode: 'DRIVING',
			avoidTolls: true
		}, function(response, status) {
			if (status === 'OK') {
				display.setDirections(response);
				console.log(response,"trayek");
				get_jln(response)
		} else {
			alert('Could not display directions due to: ' + status);
		}
	});
	}

	function computeTotalDistance(result) {
		console.log(result.routes["0"].legs["0"].steps,"result");
		get_jln(result)
		var total = 0;
		var myroute = result.routes[0];
		for (var i = 0; i < myroute.legs.length; i++) {
			total += myroute.legs[i].distance.value;
		}
		total = total / 1000;
		document.getElementById('total').innerHTML = total + ' km';
	}

	function reverse(lat,lng) {
		posts = $.ajax({
		    type: 'GET',
		    url:"https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyDMku8Cv-ld6KagEfwMCmyi4G997TAAZPo&latlng="+lat+","+lng+"&sensor=true" ,
		    async: false,
		    done: function(results) {
		        // uhm, maybe I don't even need this?
		        JSON.parse(results);
		        return results;
		    },
		    fail: function( jqXHR, textStatus, errorThrown ) {
		        console.log( 'Could not get posts, server response: ' + textStatus + ': ' + errorThrown );
		    }
		}).responseJSON;
		//console.log(posts.results["0"].address_components[1].short_name,"ini")
			return posts.results["0"].address_components[1].short_name;
	}

	function kirim() {
		formdata=new FormData();
		formdata.append("_token","{{csrf_token()}}");
		formdata.append("nomor",$('#nomor').val());
		formdata.append("mitra",$('#mitra').val());
		formdata.append("awal",awal);
		formdata.append("tujuan",tujuan);	
		formdata.append("awal_latlng",awal_latlng);
		formdata.append("tujuan_latlng",tujuan_latlng);
		formdata.append("latlng",tmp1);
		formdata.append("rute",listjalan);
		formdata.append("image",file);
		$.ajax({
			url:"/mitra/update/trayek/"+"{{$angkot->id}}",
			type:"POST",
			data:formdata,
			processData:false,
			contentType:false,
			success:function (data) {
				location.href="/mitra/trayek-angkot";
				console.log(data);
			}
		})
	}

function get_jln(response) {
	console.log(response,"ress");
		listjalan=[]
		z=[]
		tujuan_latlng=response.routes["0"].legs["0"].end_location.lat()+','+response.routes["0"].legs["0"].end_location.lng();
		awal_latlng=response.routes["0"].legs["0"].start_location.lat()+','+response.routes["0"].legs["0"].start_location.lng();
		tujuan=(response.request.destination.location)?[response.request.destination.location.lat(),response.request.destination.location.lng()]:[response.request.destination.lat(),response.request.destination.lng()];
		awal=(response.request.origin.location)?[response.request.origin.location.lat(),response.request.origin.location.lng()]:[response.request.origin.lat(),response.request.origin.lng()];
		steps=response.routes["0"].legs["0"].steps;
		console.log(steps,Array.isArray(awal),tujuan,"ws")
		$.each(steps, function( index, value ) {
					z.push(value.instructions);
				});
				for(i=0; i<z.length;i++){
					var zz = z[i].split('<b>');
				listjalan.push(zz[zz.length-1].split('</b>')[0]);
			}
			//console.log(awal,reverse(awal[0],awal[1]),"isthis")
			awal=(Array.isArray(awal))?reverse(awal[0],awal[1]):awal;
			tujuan=(Array.isArray(tujuan))?reverse(tujuan[0],tujuan[1]):tujuan;
			console.log(listjalan,awal,tujuan);
	}

</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDMku8Cv-ld6KagEfwMCmyi4G997TAAZPo&callback=initMap">
</script>
@endsection