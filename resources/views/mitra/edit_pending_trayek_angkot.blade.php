@extends('master.master_mitra')
@section('content')
<div class="content-admin" style="height: 80%">
	<div class="row ml-0 mr-0 mb-3 title-page-admin">
		<div class="col p-0">
			<div class="">Edit Trayek Angkot</div>
		</div>
		<div class="col p-0 text-right">
		<button class="btn btn-app" id="confirm" onclick="kirim()">kirim</button>
		</div>
	</div>
	<div class="row m-0 mb-5">
		<div class="col p-0" style="max-width: 15rem">
			<span class="text-bold font-16 pt-2">Nomor Angkot</span>
		</div>
		<div class="col p-0">
			<input type="number" id="nomor" class="form-control" style="width: 7rem">
		</div>
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 15rem">
			<span class="text-bold font-16 pt-2">Gambar Angkot</span>
		</div>
		<div class="col p-0">
			<div class="image-view-upload d-inline-block">
				<input name="image" type="file" accept="image/*" onchange="uploadimage(event)" id="imginput" class="hidden" />
				<img id="imgview" src="{{asset('img/angkot.jpg')}}" class="image-upload"/><br>
				<label class="mt-2 btn-change text-center" for="imginput">
					<span class="btn btn-upload">Pilih Gambar</span>
				</label>
			</div>
		</div>
	</div>
	<span class="text-bold font-16 pt-2">Jalur Angkot</span>
	<div class="row m-0" style="height: 70%">
		<div class="col p-0" id="map"></div>

		<div class="col pl-4 pr-3" id="panel">
			<p>Total Distance: <span id="total"></span></p>
		</div>
	</div>
</div>
<script>
	var steps;
	var route_arr=[];
	var tmp1 =[]
	var z =[]
	var listjalan=[]
	var file;

	$("#imginput").on("change",function () {
		file=this.files[0];
	});

	function initMap() {
		console.log("satu");
		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 4,
          center: {lat: 3.5813667411452568, lng: 98.69381740856034}  // Australia.
      });

		var directionsService = new google.maps.DirectionsService;
		var directionsDisplay = new google.maps.DirectionsRenderer({
			draggable: true,
			map: map,
			panel: document.getElementById('panel')
		});

		directionsDisplay.addListener('directions_changed', function() {
			computeTotalDistance(directionsDisplay.getDirections());

			//get latlang
			var route = directionsDisplay.getDirections().routes[0];
			var points = route.overview_path;
			for (var i = 0; i < points.length; i++) {
				route_arr.push({"location":new google.maps.LatLng(points[i].lat(),points[i].lng())});
				tmp1.push(points[i].lat()+'|'+points[i].lng());
		    	//reverse(points[i].lat(),points[i].lng());
		        //console.log(points[i].lat(),points[i].lng(),"ini");
		    }
		   // console.log(JSON.stringify(tmp1))

		});

		displayRoute('Jl. Merbabu No.43 ,  Pusat Ps. ,  Medan Kota ,  Kota Medan ,  Sumatera Utara 20211 ,  Indonesia', 'Jl. Thamrin No.76 ,  Pandau Hulu I ,  Medan Kota ,  Kota Medan ,  Sumatera Utara 20211 ,  Indonesia', directionsService,
			directionsDisplay);
	}

	function displayRoute(origin, destination, service, display) {
		console.log("dua");
		// route_arr.push({"location":new google.maps.LatLng(3.594,98.679)});
		service.route({
			origin: origin,
			destination: destination,
			waypoints:route_arr,
			travelMode: 'DRIVING',
			avoidTolls: true
		}, function(response, status) {
			if (status === 'OK') {
				display.setDirections(response);
				console.log(response,"trayek");
				steps=response.routes["0"].legs["0"].steps;
				// console.log(steps,"steps");
				
				$.each(steps, function( index, value ) {
					z.push(value.instructions);
					/*$.each(value.instructions.split('<b>'),function (i,val) {
				  		// console.log( i,val.split('</b>')[0] );
					})
					console.log( index + ": " , value.instructions );*/
				});
				// console.log(z);
				for(i=0; i<z.length;i++){
					var zz = z[i].split('<b>');
				// console.log(zz[zz.length-1].split('</b>')[0]);
				listjalan.push(zz[zz.length-1].split('</b>')[0]);
			}
			console.log(listjalan);
		} else {
			alert('Could not display directions due to: ' + status);
		}
	});
	}

	function computeTotalDistance(result) {
		console.log("tiga");
		var total = 0;
		var myroute = result.routes[0];
		for (var i = 0; i < myroute.legs.length; i++) {
			total += myroute.legs[i].distance.value;
		}
		total = total / 1000;
		document.getElementById('total').innerHTML = total + ' km';
	}

	function reverse(lat,lng) {
		$.get("http://maps.googleapis.com/maps/api/geocode/json?latlng="+lat+","+lng+"&sensor=true", function(data, status){
			console.log(data.results);
        //console.log(data.results["0"].formatted_address);
    });
	}

	function kirim() {
		formdata=new FormData();
		formdata.append("_token","{{csrf_token()}}");
		formdata.append("nomor",$('#nomor').val());
		formdata.append("latlng",tmp1);
		formdata.append("rute",listjalan);
		formdata.append("image",file);
		$.ajax({
			url:"/mitra/create/trayek",
			type:"POST",
			data:formdata,
			processData:false,
			contentType:false,
			success:function (data) {
				//location.href="";
				console.log(data);
			}
		})
	}


</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDMku8Cv-ld6KagEfwMCmyi4G997TAAZPo&callback=initMap">
</script>
@endsection