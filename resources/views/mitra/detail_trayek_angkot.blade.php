@extends('master.master_mitra')
@section('content')
<div class="content-admin" style="height: 79%">
	<div class="ml-0 mr-0 mb-3 title-page-admin">
		<div>Detail Trayek Angkot</div>
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Nama Perusahaan</span>
		</div>		
		<div class="col p-0">
			<span class="font-16 pt-2">{{$angkot->mitra->nama}}</span>
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Nomor Angkot</span>
		</div>		
		<div class="col p-0">
			<span class="font-16 pt-2">{{($angkot->status==2)?$angkot->p_nomor:$angkot->nomor}}</span>
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Gambar Angkot </span>
		</div>		
		<div class="col p-0">
			<img src="{{asset('images/angkot')}}/{{($angkot->status==2 && $angkot->p_image)?$angkot->p_image:$angkot->image}}" class="image-upload"/>
		</div>		
	</div>
	<div class="text-bold font-16 pt-2">Rute Angkot</div>
	<div class="row m-0" style="height: 70%">
		<div class="col p-0" id="map"></div>

		<div class="col pl-4 pr-3" id="panel">
			<p>Total Distance: <span id="total"></span></p>
		</div>
	</div>
</div>
<script>
	wpoin=[];
		var api_latlng='{{($angkot->status==2)?$angkot->p_latlng:$angkot->latlng}}'.split(',');
	function initMap() {
		var api_awal='{{($angkot->status==2)?$angkot->p_awal_latlng:$angkot->awal_latlng}}';
		var api_tujuan='{{($angkot->status==2)?$angkot->p_tujuan_latlng:$angkot->tujuan_latlng}}';
		//console.log(api_latlng,"latlng")
		
			//wpoin.push({"location":new google.maps.LatLng(3.5813667411452568,98.69381740856034)})
	
		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 4,
          center: {lat: 3.5813667411452568, lng: 98.69381740856034}  // Australia.
      });

		var directionsService = new google.maps.DirectionsService;
		var directionsDisplay = new google.maps.DirectionsRenderer({
			// draggable: true,
			map: map,
			panel: document.getElementById('panel')
		});

		directionsDisplay.addListener('directions_changed', function() {
			computeTotalDistance(directionsDisplay.getDirections());
		});

		displayRoute(new google.maps.LatLng(parseFloat(api_awal.split(',')[0]),parseFloat(api_awal.split(',')[1])),new google.maps.LatLng(parseFloat(api_tujuan.split(',')[0]),parseFloat(api_tujuan.split(',')[1])), directionsService,directionsDisplay);
		
	}

	function displayRoute(origin, destination, service, display) {
		$.each(api_latlng,function (i,v) {
			//console.log(v.split('|'),v,"ini")
			if(v!=''){
				var cordinat=new google.maps.LatLng(parseFloat(v.split('|')[0]),parseFloat(v.split('|')[1]))
				wpoin.push({"location":cordinat,"stopover":false})
			}
		})
/*		console.log(wpoin,api_latlng,"wpoin")
		wpoin=[];*/
		service.route({
			origin: origin,
			destination: destination,
			waypoints:wpoin,
			travelMode: 'DRIVING',
			avoidTolls: true
		}, function(response, status) {
				console.log(response,wpoin,"trayek");
			if (status === 'OK') {
				display.setDirections(response);
			} else {
				alert('Could not display directions due to: ' + status);
			}
		});
	}

	function computeTotalDistance(result) {
		var total = 0;
		var myroute = result.routes[0];
		for (var i = 0; i < myroute.legs.length; i++) {
			total += myroute.legs[i].distance.value;
		}
		total = total / 1000;
		document.getElementById('total').innerHTML = total + ' km';
	}
</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDMku8Cv-ld6KagEfwMCmyi4G997TAAZPo&callback=initMap">
</script>
@endsection