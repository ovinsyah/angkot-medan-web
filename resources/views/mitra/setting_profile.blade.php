@extends('master.master_mitra')
@section('content')
<div class="content-admin">
	<div class="row ml-0 mr-0 mb-3 title-page-admin">
		<div class="col p-0">
			<div class="">Pengaturan Profile</div>
		</div>
		<div class="col p-0 text-right">
		<a href="{{url('mitra/setting/edit/profile')}}" class="btn btn-app font-16">
				Edit profile
			</a>
		</div>
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Nama Perusahaan</span>
		</div>		
		<div class="col p-0">
			<span class="font-16 pt-2">{{$mitra->nama}}</span>
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Contact Perusahaan</span>
		</div>		
		<div class="col p-0">
			<span class="font-16 pt-2">{{$mitra->contact}}</span>
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Profile Perusahaan</span>
		</div>		
		<div class="col p-0">
		    <img src="{{asset('images/mitra')}}/{{$mitra->image}}" class="image-upload"/>
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Alamat Perusahaan</span>
		</div>		
		<div class="col p-0">
			<span class="font-16 pt-2">{{$mitra->alamat}}</span>
		</div>		
	</div>
	<div class="row m-0 mb-5">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Deskripsi Perusahaan</span>
		</div>		
		<div class="col p-0">
			<span class="font-16 pt-2">{{$mitra->deskripsi}}</span>
		</div>		
	</div>
</div>
<script type="text/javascript">
	$('#adm-setting').addClass('active');
	$('#adm-setting').removeClass('collapsed');
    $('#subsetting').attr( "aria-expanded", "true" );
	$('#subsetting').addClass('in');
	$('#subsetting').addClass('show');
	$('#adm-menu2').attr( "aria-expanded", "true" );
	$('#adm-profile').addClass('active');
</script>
@endsection