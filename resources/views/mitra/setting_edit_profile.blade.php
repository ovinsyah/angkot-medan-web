@extends('master.master_mitra')
@section('content')
<div class="content-admin">
	<form method="post" action="{{url('mitra/setting/update/profile')}}" enctype="multipart/form-data" id="form">
		{{csrf_field()}}
	<div class="row ml-0 mr-0 mb-3 title-page-admin">
		<div class="col p-0">
			<div class="">Edit Profile</div>
		</div>
		<div class="col p-0 text-right">
		<button id="confirm" class="btn btn-app font-16">
				Simpan
			</button>
		</div>
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Nama Perusahaan</span>
		</div>		
		<div class="col p-0">
			<input type="" name="nama" class="form-control" style="width: 30rem" value="{{$mitra->nama}}" required>
			<input type="hidden" name="email" class="form-control" style="width: 30rem" value="{{$mitra->email}}">
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Contact Perusahaan</span>
		</div>		
		<div class="col p-0">
			<input type="" name="contact" class="form-control" style="width: 30rem" value="{{$mitra->contact}}" required>
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Profile Perusahaan</span>
		</div>		
		<div class="col p-0">
		    <div class="image-view-upload d-inline-block">
			<input name="image" type="file" accept="image/*" onchange="uploadimage(event)" id="imginput" class="hidden" />
		    <img id="imgview" src="{{asset('images/mitra')}}/{{$mitra->image}}" class="image-upload"/><br>
		    <label class="mt-2 btn-change text-center" for="imginput">
		      <span class="btn btn-upload">Pilih Gambar</span>
		    </label>
			</div>
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Alamat Perusahaan</span>
		</div>		
		<div class="col p-0">
			<textarea class="form-control" rows="6" name="alamat" required>{{$mitra->alamat}}</textarea>
		</div>		
	</div>
	<div class="row m-0 mb-5">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Deskripsi Perusahaan</span>
		</div>		
		<div class="col p-0">
			<textarea class="form-control" rows="6" name="deskripsi" required>{{$mitra->deskripsi}}</textarea>
		</div>		
	</div>
	</form>
</div>
<script type="text/javascript">
	$('#adm-setting').addClass('active');
	$('#adm-setting').removeClass('collapsed');
    $('#subsetting').attr( "aria-expanded", "true" );
	$('#subsetting').addClass('in');
	$('#subsetting').addClass('show');
	$('#adm-menu2').attr( "aria-expanded", "true" );
	$('#adm-profile').addClass('active');
</script>
@endsection