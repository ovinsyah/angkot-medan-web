@extends('master.master_mitra')
@section('content')
<div class="content-admin">
	@if ($message = Session::get('error'))
	<div class="alert alert-danger alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button> 
		<strong>{{ $message }}</strong>
	</div>
	@endif
	<form method="post" action="{{url('mitra/setting/update/password')}}" id="form">
		{{csrf_field()}}
	<div class="row ml-0 mr-0 mb-5 title-page-admin">
		<div class="col p-0">
			<div class="">Ganti Password</div>
		</div>
		<div class="col p-0 text-right">
		<button id="confirm" class="btn btn-app font-16">
			Simpan
			</button>
		</div>
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Password Lama</span>
		</div>		
		<div class="col p-0">
			<input type="password" name="old_password" class="form-control password" style="width: 30rem">
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Password Baru</span>
		</div>		
		<div class="col p-0">
			<input type="password" name="new_password" class="form-control password" style="width: 30rem" id="pass">
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Konfirmasi Password Baru</span>
		</div>		
		<div class="col p-0">
			<input type="password" name="re_password" class="form-control password" style="width: 30rem" id="repass">
		</div>		
	</div>
	</form>
</div>
<script type="text/javascript">
	$('#adm-setting').addClass('active');
	$('#adm-setting').removeClass('collapsed');
    $('#subsetting').attr( "aria-expanded", "true" );
	$('#subsetting').addClass('in');
	$('#subsetting').addClass('show');
	$('#adm-menu2').attr( "aria-expanded", "true" );
	$('#adm-account').addClass('active');

	$('#form').on('click change keyup',function () {
		if($('#pass').val() != $('#repass').val()){
			$('#pass,#repass').addClass('form-error');
			$('#confirm').addClass('disabled')
		}
		else{
			$('#pass,#repass').removeClass('form-error');
			$('#confirm').removeClass('disabled')
		}
	});

</script>
@endsection