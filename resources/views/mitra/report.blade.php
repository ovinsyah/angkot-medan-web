@extends('master.master_mitra')
@section('content')
<div class="content-admin">
	<div class="title-page-admin mb-3">Laporan</div>
	<table id="table_id" class="display">
		<thead>
			<tr>
				<th>Nama Pengguna</th>
				<th>Nomor Angkot</th>
				<th>Nomor Polisi</th>
				<th>Perusahaan</th>
				<th>Tanggal</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			@foreach($laporans as $laporan)
			<tr>
				<td><a href="{{url('mitra/detail/report',$laporan->id)}}">{{$laporan->user->name}}</a></td>
				<td><a href="{{url('mitra/detail/report',$laporan->id)}}">{{$laporan->angkot->nomor}}</a></td>
				<td><a href="{{url('mitra/detail/report',$laporan->id)}}">{{$laporan->nopol}}</a></td>
				<td><a href="{{url('mitra/detail/report',$laporan->id)}}">{{$laporan->mitra->nama}}</a></td>
				<td><a href="{{url('mitra/detail/report',$laporan->id)}}">{{$laporan->created_at}}</a></td>
				<td>
					<a href="{{url('mitra/detail/report',$laporan->id)}}" class="btn btn-app-icon-1">
						<span class="glyphicon glyphicon-eye-open"></span>
					</a>
					<!-- <a href="" class="btn btn-app-icon-2" data-toggle="modal" data-target="#modalRemove{{$laporan->id}}">
						<span class="glyphicon glyphicon-trash"></span>
					</a> -->
				</td>
			</tr>
						<!-- Modal -->
			<div class="modal fade" id="modalRemove{{$laporan->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="myModalLabel">Hapus Rute</h4>
			      </div>
			      <div class="modal-body">
			        Apakah anda yakin menghapus laporan angkot no {{$laporan->angkot->nomor}} ?
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
			        <a href="{{url('mitra/hapus/laporan')}}" class="btn btn-primary">Ya</a>
			      </div>
			    </div>
			  </div>
			</div>
			@endforeach
		</tbody>
	</table>
</div>
<script type="text/javascript">
	$('#table_id').DataTable();
</script>
@endsection