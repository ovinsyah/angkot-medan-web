@extends('master.master_mitra')
@section('content')
<div class="content-admin">
	<div class="row ml-0 mr-0 mb-3 title-page-admin">
		<div class="col p-0">
			<div class="">Trayek Angkot</div>
		</div>
		<div class="col p-0 text-right">
		<a href="{{url('mitra/add/trayek-angkot')}}" class="btn btn-app font-16">
				<span class="glyphicon glyphicon-plus mr-2"></span>Trayek
			</a>
		</div>
	</div>
	<table id="table_id" class="display">
		<thead>
			<tr>
				<th>Nomor Angkot</th>
				<th>Rute</th>
				<th>Tanggal</th>
				<th width="100">Aksi</th>
			</tr>
		</thead>
		<tbody>
			@foreach($angkots as $angkot)
			<tr>
				<td><a href="{{url('mitra/detail/trayek-angkot',$angkot->id)}}">{{$angkot->nomor}}</a></td>
				<td><a class="ellipsis-3" href="{{url('mitra/detail/trayek-angkot',$angkot->id)}}">{{$angkot->rutes}}</a></td>
				<td><a href="{{url('mitra/detail/trayek-angkot',$angkot->id)}}">{{$angkot->created_at}}</a></td>
				<td>
					<a href="{{url('mitra/detail/trayek-angkot',$angkot->id)}}" class="btn btn-app-icon-1">
						<span class="glyphicon glyphicon-eye-open"></span>
					</a>
					@if($angkot->status!=2 )
					<a  href="{{url('mitra/edit/trayek-angkot',$angkot->id)}}" class="btn btn-app-icon-3">
						<span class="glyphicon glyphicon-pencil"></span>
					</a>
					@endif
					@if($angkot->status!=3 )
					<a href="" class="btn btn-app-icon-2" data-toggle="modal" data-target="#modalRemove{{$angkot->id}}">
						<span class="glyphicon glyphicon-trash"></span>
					</a>
					@endif
				</td>
			</tr>
			<!-- Modal -->
			<div class="modal fade" id="modalRemove{{$angkot->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="myModalLabel">Hapus Angkot</h4>
			      </div>
			      <div class="modal-body">
			        Apakah anda yakin menghapus trayek {{$angkot->nomor}} ?
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
			        <a href="{{url('mitra/delete/trayek',$angkot->id)}}" class="btn btn-primary">Ya</a>
			      </div>
			    </div>
			  </div>
			</div>
			@endforeach
		</tbody>
	</table>
</div>

<script type="text/javascript">
	$(document).ready( function () {
    $('#table_id').DataTable();

    
	$('#adm-trayek').addClass('active');
	$('#adm-trayek').removeClass('collapsed');
    $('#pageSubmenu').attr( "aria-expanded", "true" );
	$('#pageSubmenu').addClass('in');
	$('#pageSubmenu').addClass('show');
	$('#adm-menu').attr( "aria-expanded", "true" );
	$('#adm-tr-angkot').addClass('active');
} );
</script>
@endsection