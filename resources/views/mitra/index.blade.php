@extends('master.master_mitra')
@section('content')
<div class="content-admin">
	<div class="title-page-admin mb-5">Dashboard</div>
	<div class="row m-0">
		<div class="col">
			<a href="{{url('mitra/trayek-angkot')}}">
				<div class="row m-0 col-dash-mitra">
					<div class="col p-2">
						<img src="{{asset('img/rute.png')}}" style="width: 80%">
					</div>
					<div class="col text-center pt-4">
						<h1 class="m-0 text-bold">{{count(App\Angkot::where('status',1)->where('mitra_id',Auth::guard('mitra')->user()->id)->get())}}</h1>
						<span class="font-16 mt-2 text-bold">Trayek Angkot Disetujui</span>
					</div>
				</div>
			</a>
		</div>
		<div class="col">
			<a href="{{url('mitra/pending/trayek-angkot')}}">
				<div class="row m-0 col-dash-mitra">
					<div class="col p-2">
						<img src="{{asset('img/rute-pending.png')}}" style="width: 80%">
					</div>
					<div class="col text-center pt-4">
						<h1 class="m-0 text-bold">{{count(App\Angkot::where('status',0)->where('mitra_id',Auth::guard('mitra')->user()->id)->get())}}</h1>
						<span class="font-16 mt-2 text-bold">Trayek Angkot Pending</span>
					</div>
				</div>
			</a>
		</div>
		<div class="col">
			<a href="{{url('mitra/report')}}">
			<div class="row m-0 col-dash-mitra">
				<div class="col p-2">
					<img src="{{asset('img/report.png')}}" style="width: 80%">
				</div>
				<div class="col text-center pt-4">
					<h1 class="m-0 text-bold">{{count(Auth::user()->laporan)}}</h1>
					<span class="font-16 mt-2 text-bold">Laporan Pengguna</span>
				</div>
			</div>
		</a>
		</div>
	</div>
</div>
@endsection