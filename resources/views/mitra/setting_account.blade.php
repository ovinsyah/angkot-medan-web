@extends('master.master_mitra')
@section('content')
<div class="content-admin">
	<div class="row ml-0 mr-0 mb-3 title-page-admin">
		<div class="col p-0">
			<div class="">Pengaturan Akun</div>
		</div>
		<div class="col p-0 text-right">
		<a href="{{url('mitra/setting/change-password')}}" class="btn btn-app font-16">
				Ganti Password
			</a>
		</div>
	</div>
   @if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong>{{ $message }}</strong>
    </div>
  @endif
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Email Perusahaan</span>
		</div>		
		<div class="col p-0">
			<span class="font-16 pt-2">{{Auth::user()->email}}</span>
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Password</span>
		</div>		
		<div class="col p-0">
			<span class="font-16 pt-2">*********</span>
		</div>		
	</div>
</div>
<script type="text/javascript">
	$('#adm-setting').addClass('active');
	$('#adm-setting').removeClass('collapsed');
    $('#subsetting').attr( "aria-expanded", "true" );
	$('#subsetting').addClass('in');
	$('#subsetting').addClass('show');
	$('#adm-menu2').attr( "aria-expanded", "true" );
	$('#adm-account').addClass('active');
</script>
@endsection