@extends('master.master_mitra')
@section('content')
<div class="content-admin" style="height: 80%">
	<div class="row ml-0 mr-0 mb-3 title-page-admin">
		<div class="col p-0">
			<div class="">Tambah Trayek Angkot</div>
		</div>
		<div class="col p-0 text-right">
		<button class="btn btn-app" id="confirm" onclick="kirim()">kirim</button>
		</div>
	</div>
	<div class="row m-0 mb-5">
		<div class="col p-0" style="max-width: 15rem">
			<span class="text-bold font-16 pt-2">Nomor Angkot</span>
		</div>
		<div class="col p-0">
			<input type="number" id="nomor" class="form-control" style="width: 7rem">
			<input type="hidden" id="mitra" value="{{Auth::user()->id}}" ">
		</div>
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 15rem">
			<span class="text-bold font-16 pt-2">Gambar Angkot</span>
		</div>
		<div class="col p-0">
			<div class="image-view-upload d-inline-block">
				<input name="image" type="file" accept="image/*" onchange="uploadimage()" id="imginput" class="hidden" />
				<img id="imgview" src="{{asset('img/angkot.jpg')}}" class="image-upload"/><br>
				<label class="mt-2 btn-change text-center" for="imginput">
					<span class="btn btn-upload">Pilih Gambar</span>
				</label>
			</div>
		</div>
	</div>
	<span class="text-bold font-16 pt-2">Jalur Angkot</span>
	<div class="row m-0" style="height: 70%">
		<div class="col p-0" id="map"></div>

		<div class="col pl-4 pr-3" id="panel">
			<p>Total Distance: <span id="total"></span></p>
		</div>
	</div>
</div>
<script>
	var steps;
	var route_arr=[];
	var tmp1 =[]
	var z =[]
	var listjalan=[]
	var file;
	var awal;
	var tujuan;
	var awal_latlng;
	var tujuan_latlng;
	var dir_dis;
	var directionsService;
	var directionsDisplay;

	$("#imginput").on("change",function () {
		file=this.files[0];
	});

	function initMap() {
		console.log("satu");
		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 4,
          center: {lat: 3.5813667411452568, lng: 98.69381740856034}  // Australia.
      });

		directionsService = new google.maps.DirectionsService;
		directionsDisplay = new google.maps.DirectionsRenderer({
			draggable: true,
			map: map,
			panel: document.getElementById('panel')
		});

			/*dir_dis=directionsDisplay.getDirections();
			console.log(dir_dis,"dir_dis2");*/
		directionsDisplay.addListener('directions_changed', function() {
			computeTotalDistance(directionsDisplay.getDirections());
			//get latlang
			var route = directionsDisplay.getDirections().routes[0];
			var points = route.overview_path;
			for (var i = 0; i < points.length; i++) {
				route_arr.push({"location":new google.maps.LatLng(points[i].lat(),points[i].lng())});
				tmp1.push(points[i].lat()+'|'+points[i].lng());
		    	//reverse(points[i].lat(),points[i].lng());
		        //console.log(points[i].lat(),points[i].lng(),"ini");
		    }

		});

		displayRoute('Jl. Merbabu No.43 ,  Pusat Ps. ,  Medan Kota ,  Kota Medan ,  Sumatera Utara 20211 ,  Indonesia', 'Jl. Thamrin No.76 ,  Pandau Hulu I ,  Medan Kota ,  Kota Medan ,  Sumatera Utara 20211 ,  Indonesia', directionsService,
			directionsDisplay,dir_dis);
	}

	function displayRoute(origin, destination, service, display,d) {
		//console.log(display,"dir_dis");
		console.log(route_arr);
		// route_arr.push({"location":new google.maps.LatLng(3.594,98.679)});
		service.route({
			origin: origin,
			destination: destination,
			//waypoints:route_arr,
			travelMode: 'DRIVING',
			avoidTolls: true
		}, function(response, status) {
			if (status === 'OK') {
				display.setDirections(response);
				console.log(response,"trayek");
				 get_jln(response)
				
				
		} else {
			alert('Could not display directions due to: ' + status);
		}
	});
	}

	function computeTotalDistance(result) {
		console.log(result.routes["0"].legs["0"].steps,"result");
		get_jln(result)

		var total = 0;
		var myroute = result.routes[0];
		for (var i = 0; i < myroute.legs.length; i++) {
			total += myroute.legs[i].distance.value;
		}
		total = total / 1000;
		document.getElementById('total').innerHTML = total + ' km';
	}

	function reverse(lat,lng) {
		posts = $.ajax({
		    type: 'GET',
		    url:"https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyDMku8Cv-ld6KagEfwMCmyi4G997TAAZPo&latlng="+lat+","+lng+"&sensor=true" ,
		    async: false,
		    done: function(results) {
		        // uhm, maybe I don't even need this?
		        JSON.parse(results);
		        return results;
		    },
		    fail: function( jqXHR, textStatus, errorThrown ) {
		        console.log( 'Could not get posts, server response: ' + textStatus + ': ' + errorThrown );
		    }
		}).responseJSON;
		//console.log(posts.results["0"].address_components[1].short_name,"ini")
			return posts.results["0"].address_components[1].short_name;
	}

	function kirim() {
		formdata=new FormData();
		formdata.append("_token","{{csrf_token()}}");
		formdata.append("nomor",$('#nomor').val());
		formdata.append("mitra",$('#mitra').val());
		formdata.append("awal",awal);
		formdata.append("tujuan",tujuan);	
		formdata.append("awal_latlng",awal_latlng);
		formdata.append("tujuan_latlng",tujuan_latlng);
		formdata.append("latlng",tmp1);
		formdata.append("rute",listjalan);
		formdata.append("image",file);
		$.ajax({
			url:"/mitra/create/trayek",
			type:"POST",
			data:formdata,
			processData:false,
			contentType:false,
			success:function (data) {
				//location.href="";
				console.log(data);
			}
		})
	}

	function get_jln(response) {
		listjalan=[]
		z=[]
		tujuan_latlng=response.routes["0"].legs["0"].end_location.lat()+','+response.routes["0"].legs["0"].end_location.lng();
		awal_latlng=response.routes["0"].legs["0"].start_location.lat()+','+response.routes["0"].legs["0"].start_location.lng();
		tujuan=(response.request.destination.query)?response.request.destination.query.split(',')[0].split(' No')[0]:[response.request.destination.lat(),response.request.destination.lng()];
		awal=(response.request.origin.query)?response.request.origin.query.split(',')[0].split(' No')[0]:[response.request.origin.lat(),response.request.origin.lng()];
		steps=response.routes["0"].legs["0"].steps;
		console.log(steps,Array.isArray(awal),tujuan,"ws")
		$.each(steps, function( index, value ) {
					z.push(value.instructions);
				});
				for(i=0; i<z.length;i++){
					var zz = z[i].split('<b>');
				listjalan.push(zz[zz.length-1].split('</b>')[0]);
			}
			//console.log(awal,reverse(awal[0],awal[1]),"isthis")
			awal=(Array.isArray(awal))?reverse(awal[0],awal[1]):awal;
			tujuan=(Array.isArray(tujuan))?reverse(tujuan[0],tujuan[1]):tujuan;
			console.log(listjalan,awal,tujuan);
	}


</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDMku8Cv-ld6KagEfwMCmyi4G997TAAZPo&callback=initMap">
</script>
@endsection