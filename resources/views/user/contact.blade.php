@extends('master.master_user')
@section('content')
<div class="col-app-user m-auto">
	<h3 class="mb-5">Kamu dapat menghubungi kami di</h3>

	@foreach($contacts as $contact)
	<div class="row m-0">
		<div class="col p-0" style="max-width: 9rem">
			<img src="{{asset('images/contact')}}/{{$contact->icon}}" style="width: 57px">
		</div>
		<div class="col p-0 font-18">
		<div class="text-bold">{{$contact->judul}}</div>
			<span>{{$contact->deskripsi}}</span>
		</div>
	</div>
	@endforeach

</div>
@endsection