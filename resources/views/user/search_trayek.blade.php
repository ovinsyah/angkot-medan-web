@extends('master.master_user')
@section('content')
<div class="col-app-user m-auto">
	<h3 class="mb-5 mt-5">Trayek dari {{$awal}} ke {{$tujuan}}</h3>
	<div class="row" style="margin: -11px">
		@foreach($result as $angkot)
		<div class="col-md-6 col-lg-4 mb-5">
			<div class="row m-0 col-item-search">
				<div class="col-md-4 p-4 text-center">
					<div class="text-bold font-24">No {{$angkot->nomor}}</div>
					<div class="font-20">{{$angkot->mitra->nama}}</div>
					<img src="{{asset('img/angkot.png')}}" style="max-height: 18rem">
				</div>
				<div class="col-md-8 p-4" style="padding-bottom: 6rem !important;">
					<div class="text-bold font-22 mb-3">Rute angkot:</div>
					<span class="font-14 ellipsis-5 text-justify">{{$angkot->rutes}}</span>
					<div class="detail-maps">
						<a href="{{url('detail/trayek',$angkot->id)}}">Detail Maps</a>
					</div>
				</div>	
			</div>
		</div>
		@endforeach
	</div>
</div>
@endsection