@extends('master.master_user')
@section('content')
<style type="text/css">
	.easy-autocomplete-container ul{
		top: 52px !important;
		background: none !important;
	}
	.easy-autocomplete-container ul li{
		padding: 10px 20px !important;
	    background: white !important;
	    margin-bottom: 2px !important;
	}
</style>
<div class="col-app-user m-auto">
	<div class="input-group" style="width: 100%;">
		<form method="get" action="{{url('search-angkot')}}">
	      <input type="text" class="form-control search-app-angkot" value="{{$id}}" id="search" name="angkot" placeholder="Cari Angkot" style="width: 92% !important;">
	      <span class="input-group-btn">
	        <button  class="btn btn-app btn-app-angkot" type="submit">Cari</button>
	      </span>
		</form>
    </div>
	<div class="row m-0 mt-5">
		@foreach($angkots as $angkot)
		<div class="p-2 col-lg-3 col-md-4 col-sm-6">
			<a href="{{url('detail/trayek',$angkot->id)}}" style="text-decoration: none;">
			<div class="bg-form-angkot row m-0 p-4">
				<div class="col p-0">
					<h2 class="m-0">No {{$angkot->nomor}}</h2>
					<div class="font-16">{{$angkot->mitra->nama}}</div>
					<div class="mt-2">Rute :</div>
					<span>{{$angkot->rutes}}</span>
				</div>
				<div class="col p-0">
					<img src="{{asset('images/angkot')}}/{{$angkot->image}}">
				</div>
			</div>
		</a>
		</div>
		@endforeach
	</div>
</div>
@endsection