@extends('master.master_user')
@section('content')
<div class="bg-home text-center col-home m-auto" id="form">
	<img src="{{asset('img/angkot.png')}}" style="width: 30rem">
	<h1>Mau Kemana Dek ?</h1>
	<p class="font-16 ellipsis-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
	consequat.</p>
	<div class="row m-auto" style="width: 70%">
		<div class="col-md-4 mb-2 pr-2">
			<input type="" name="awal" class="form-control search-app" placeholder="Asal" id="start-loc">
		</div>
		<div class="col-md-4 mb-2 pr-2">
			<input type="" name="tujuan" class="form-control search-app" placeholder="Tujuan" id="end-loc">
		</div>
		<div class="col-md-4 mb-2 pr-2">
			<a onclick="ini()" class="btn btn-search disabled" id="confirm">Cari Angkot </a>
		</div>
	</div>
</div>
<script type="text/javascript">
	//cek
	$('#form').on('click change keyup',function () {
		if($('#start-loc').val() == '' || $('#end-loc').val() == ''){
			$('#confirm').addClass('disabled');
		}
		else{
			$('#confirm').removeClass('disabled');
		}
	})
	//search-trayek
	function ini() {
		var awal=$('[name=awal]').val()
		var tujuan=$('[name=tujuan]').val()
		 location.href="/search-trayek/"+awal+"/"+tujuan;
		console.log(awal,tujuan)
	}
	var start = {
		url:'/uniq',
		list: {
			match: {
				enabled: true
			}
		}
	};
	var end = {
		url:'/uniq',
		list: {
			match: {
				enabled: true
			}
		}
	};
	$("#start-loc").easyAutocomplete(start);
	$("#end-loc").easyAutocomplete(end);
</script>
@endsection