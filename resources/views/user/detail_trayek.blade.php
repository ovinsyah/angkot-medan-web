@extends('master.master_user')
@section('content')
<div class="col-app-user m-auto">
	<h3 class="mb-3">Detail Trayek Angkot No {{$angkot->nomor}}</h3>
	<div class="row m-0 p-4" style="background: whitesmoke;">
		<div class="col pl-0">
			<img src="{{asset('images/angkot')}}/{{$angkot->image}}" style="width: 200px;height: 200px;object-fit: cover;border-radius: 50%;">
			<div class="font-18 text-bold text-center" style="width: 200px">{{$angkot->mitra->nama}}</div>
		</div>
		<div class="col pr-0">
			<div class="text-bold font-16 mb-2">Laporan Pengguna</div>
			<div style="height: 200px;overflow-y: scroll;">
				@foreach($laporans as $laporan)
				<div class="row m-0 mb-2" style="background: #eaeaea;padding: 5px;">
					<div class="col p-0" style="max-width: 40px">
						@if($laporan->user->image)
						<img src="{{asset('images/user')}}/{{$laporan->user->image}}" style="height: 30px;width: 30px;object-fit: cover;border-radius: 50%;">
						@else
						<img src="{{asset('images/user/profile.png')}}" style="height: 30px;width: 30px;object-fit: cover;border-radius: 50%;">
						@endif

					</div>
					<div class="col p-0">
						<div class="font-16">{{$laporan->isi}}</div>
						<div class="font-12 text-bold">{{$laporan->tanggal}}</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0">
			<div class="text-bold font-16 pt-2">Rute Angkot</div>
		</div>
		<div class="col p-0 text-right mt-2">
			@guest
			<a href="{{url('/login')}}" class="btn btn-app">Laporkan</a>	
			@else
			<a href="{{url('/report/trayek',$angkot->id)}}" class="btn btn-app">Laporkan</a>	
			@endguest
		</div>
	</div>
	<div class="row m-0">
		<div class="col p-0" id="map" style="min-height: 50rem;"></div>
		<div class="col pl-4 pr-3" id="panel">
			<p>Total Distance: <span id="total"></span></p>
		</div>
	</div>
</div>
<script>
	wpoin=[];
	var api_latlng='{{$angkot->latlng}}'.split(',');
	function initMap() {
		var api_awal='{{$angkot->awal_latlng}}';
		var api_tujuan='{{$angkot->tujuan_latlng}}';
		//console.log(api_latlng)
		
			//wpoin.push({"location":new google.maps.LatLng(3.5813667411452568,98.69381740856034)})

			var map = new google.maps.Map(document.getElementById('map'), {
				zoom: 4,
          center: {lat: 3.5813667411452568, lng: 98.69381740856034}  // Australia.
      });

			var directionsService = new google.maps.DirectionsService;
			var directionsDisplay = new google.maps.DirectionsRenderer({
			// draggable: true,
			map: map,
			panel: document.getElementById('panel')
		});

			directionsDisplay.addListener('directions_changed', function() {
				computeTotalDistance(directionsDisplay.getDirections());

			});

			displayRoute(new google.maps.LatLng(parseFloat(api_awal.split(',')[0]),parseFloat(api_awal.split(',')[1])),new google.maps.LatLng(parseFloat(api_tujuan.split(',')[0]),parseFloat(api_tujuan.split(',')[1])), directionsService,directionsDisplay);

		}

		function displayRoute(origin, destination, service, display) {
			$.each(api_latlng,function (i,v) {
				if(v!=''){
					var cordinat=new google.maps.LatLng(parseFloat(v.split('|')[0]),parseFloat(v.split('|')[1]))
					wpoin.push({"location":cordinat,"stopover":false})
				}
			})
			console.log(wpoin)
			service.route({
				origin: origin,
				destination: destination,
				waypoints:wpoin,
				travelMode: 'DRIVING',
				avoidTolls: true
			}, function(response, status) {
				console.log(response,wpoin,"trayek");
				if (status === 'OK') {
					display.setDirections(response);
				} else {
					alert('Could not display directions due to: ' + status);
				}
			});
		}

		function computeTotalDistance(result) {
			var total = 0;
			var myroute = result.routes[0];
			for (var i = 0; i < myroute.legs.length; i++) {
				total += myroute.legs[i].distance.value;
			}
			total = total / 1000;
			document.getElementById('total').innerHTML = total + ' km';
		}
	</script>
	<script async defer
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDMku8Cv-ld6KagEfwMCmyi4G997TAAZPo&callback=initMap">
</script>
@endsection