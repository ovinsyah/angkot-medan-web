@extends('master.master_user')
@section('content')

<div class="col-app-user m-auto">
	@if ($success=Session::get('success'))
    <div class="col-6 m-auto font-30 text-center alert alert-success alert-block" style="margin-top: 23vh !important;">
    <strong>Permohonan anda akan segera diproses, mohon tunggu informasi selanjutnya</strong>
    </div>
   @else
	<i>
		Untuk mitra yang ingin bergabung dengan kami dapat mengisi beberapa form yang kami ajukan. Kami akan menghubungi Bapak/Ibu untuk meninjau perusahaan  Bapak/Ibu.
	</i>
   		<form method="post" action="{{url('join-mitra')}}" enctype="multipart/form-data" id="form">
		{{csrf_field()}}
		<div class="row m-0 mb-3 mt-5">
			<div class="col p-0" style="max-width: 200px">
				<span class="text-bold font-16 pt-2">Nama Perusahaan</span>
			</div>		
			<div class="col p-0">
				<input type="" name="nama" class="form-control" style="width: 30rem" required>
			</div>		
		</div>
		<div class="row m-0 mb-3 {{ $errors->has('email') ? ' has-error' : '' }}">
			<div class="col p-0" style="max-width: 200px">
				<span class="text-bold font-16 pt-2">Email Perusahaan</span>
			</div>		
			<div class="col p-0">
				<input type="email" name="email" class="form-control" style="width: 30rem" required>
			</div>	
			@if ($errors->has('email'))
			    <span class="help-block">
			        <strong>Email Telah Digunakan</strong>
			    </span>
			@endif	
		</div>
		<div class="row m-0 mb-3">
			<div class="col p-0" style="max-width: 200px">
				<span class="text-bold font-16 pt-2">Contact Perusahaan</span>
			</div>		
			<div class="col p-0">
				<input type="telp" name="contact" class="form-control" style="width: 30rem" required>
			</div>		
		</div>
		<div class="row m-0 mb-3">
			<div class="col p-0" style="max-width: 200px">
				<span class="text-bold font-16 pt-2">Profile Perusahaan</span>
			</div>		
			<div class="col p-0">
				<div class="image-view-upload d-inline-block">
					<input name="image" type="file" accept="image/*" onchange="uploadimage(event)" id="imginput" class="hidden" required />
					<img id="imgview" src="{{asset('img/angkot.jpg')}}" class="image-upload"/><br>
					<label class="mt-2 btn-change text-center" for="imginput">
						<span class="btn btn-upload">Pilih Gambar</span>
					</label>
				</div>
			</div>		
		</div>
		<div class="row m-0 mb-3">
			<div class="col p-0" style="max-width: 200px">
				<span class="text-bold font-16 pt-2">Surat pendukung</span>
			</div>		
			<div class="col p-0">
				<div class="image-view-upload d-inline-block">
					<input name="bukti" type="file" accept="image/*" onchange="uploadimage2(event)" id="imginput2" class="hidden" required />
					<img id="imgview2" src="{{asset('img/document.png')}}" class="image-upload"/><br>
					<label class="mt-2 btn-change text-center" for="imginput2">
						<span class="btn btn-upload">Pilih Gambar</span>
					</label>
				</div>
			</div>		
		</div>
		<div class="row m-0 mb-3">
			<div class="col p-0" style="max-width: 200px">
				<span class="text-bold font-16 pt-2" >Alamat Perusahaan</span>
			</div>		
			<div class="col p-0">
				<textarea class="form-control" rows="6" name="alamat" required></textarea>
			</div>		
		</div>
		<div class="row m-0 mb-5">
			<div class="col p-0" style="max-width: 200px">
				<span class="text-bold font-16 pt-2">Deskripsi Perusahaan</span>
			</div>		
			<div class="col p-0">
				<textarea class="form-control" rows="6" name="deskripsi" required></textarea>
			</div>		
		</div>
		<div class="text-right mb-5">
			<button type="submit" class="btn btn-app disabled" id="confirm">Kirim Permintaan</button>
		</div>
	</form>
  @endif

</div>
<script type="text/javascript">
	$('#form').on('click change keyup',function () {
		if($('#imginput').val() == ''){
			$('#imgview').addClass('form-error');
		}
		else{
			$('#imgview').removeClass('form-error');
			$('#confirm').removeClass('disabled');
		}
		if($('#imginput2').val() == ''){
			$('#imgview2').addClass('form-error');
		}
		else{
			$('#imgview2').removeClass('form-error');
			$('#confirm').removeClass('disabled');
		}
	})
</script>
@endsection