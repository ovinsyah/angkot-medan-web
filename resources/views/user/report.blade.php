@extends('master.master_user')
@section('content')
<div class="col-app-user m-auto">
	<h3 class="mb-0">Detail Trayek Angkot No {{$angkot->nomor}}</h3>
	<div class="font-16">{{$angkot->mitra->nama}}</div>
	<img src="{{asset('images/angkot')}}/{{$angkot->image}}" style="width: 200px">

	<form method="post" action="{{url('/report/trayek',$angkot->id)}}" enctype="multipart/form-data">
		{{csrf_field()}}
	<div class="row m-0 mb-3">
		<div class="col-md-3 col-lg-2 p-0">
			<span class="text-bold font-16 pt-2">Nama Penumpang</span>
		</div>		
		<div class="col-md-9 col-lg-10 p-0">
			{{Auth::user()->name}}
			<input type="hidden" value="{{Auth::user()->id}}" name="user_id" class="form-control" style="width: 30rem" required id="name">
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col-md-3 col-lg-2 p-0">
			<span class="text-bold font-16 pt-2">Nomor Angkot</span>
		</div>		
		<div class="col-md-9 col-lg-10 p-0">
			<input disabled  name="nomor" class="form-control" style="width: 8rem" value="{{$angkot->nomor}}">
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col-md-3 col-lg-2 p-0">
			<span class="text-bold font-16 pt-2">Waktu</span>
		</div>		
		<div class="col-md-9 col-lg-10 p-0">
			<div class="form-group">
				<div class='input-group date' id='datetimepicker1' style="max-width: 25rem">
					<input type='text' class="form-control" name="tanggal" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col-md-3 col-lg-2 p-0">
			<span class="text-bold font-16 pt-2">Nomor Polisi</span>
		</div>		
		<div class="col-md-9 col-lg-10 p-0">
			<input type="" name="nopol" class="form-control" style="width: 12rem" placeholder="BK Nopol MS" required>
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col-md-3 col-lg-2 p-0">
			<span class="text-bold font-16 pt-2">Isi Laporan</span>
		</div>		
		<div class="col-md-9 col-lg-10 p-0">
			<textarea class="form-control" rows="6" name="isi" required></textarea>
		</div>		
	</div>
	<div class="text-right mt-3">
		<button type="submit" class="btn btn-app" id="confirm">Laporkan</button>
	</div>
	</form>
</div>
<script type="text/javascript">
	$('#datetimepicker1').datetimepicker({
		format: 'YYYY-MM-DD HH:mm'
	});
</script>
@endsection