@extends('master.master_user')
@section('content')
<div class="col-app-user m-auto">
	<div class="col-6 m-auto text-center">
			<input name="image" type="file" accept="image/*" onchange="uploadimage()" id="imginput" class="hidden" />
			<img id="imgview" src="{{asset('images/user')}}/{{(Auth::user()->image)?Auth::user()->image:'profile.png'}}" class="image-view"/><br>
			<label class="mt-2 text-center" for="imginput">
				<span class="btn bg-white">Pilih Gambar</span>
			</label>
	</div>
</div>
@endsection