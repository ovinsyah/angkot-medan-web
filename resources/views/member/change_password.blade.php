@extends('master.master_user')
@section('content')
<div class="col-app-user m-auto" id="formApp">
	@if ($message = Session::get('error'))
	<div class="alert alert-danger alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button> 
		<strong>{{ $message }}</strong>
	</div>
	@endif
	<div class="m-auto text-bold p-4" style="max-width: 50rem">
		<div class="text-center">
			<img id="imgview" src="{{asset('images/user')}}/{{(Auth::user()->image)?Auth::user()->image:'profile.png'}}" class="image-view"/><br>
		</div>
	<form action="{{url('/user/update/password/user')}}" method="post" enctype="multipart/form-data">
			{{csrf_field()}}
		<div class="row m-0 mt-3">
			<div class="col pl-0 font-16 pt-3 text-right" style="max-width: 150px">Password Lama</div>
			<div class="col pr-0 pt-2"><input id="old_pass" type="password" name="old" class="form-control"></div>
		</div>
		<div class="row m-0 mt-3">
			<div class="col pl-0 font-16 pt-3 text-right" style="max-width: 150px">Password Baru</div>
			<div class="col pr-0 pt-2"><input id="pass" type="password" name="new" class="form-control"></div>
		</div>
		<div class="row m-0 mt-3">
			<div class="col pl-0 font-16 pt-3 text-right" style="max-width: 150px">Konfirmasi Password</div>
			<div class="col pr-0 pt-2"><input id="repass" type="password" name="" class="form-control"></div>
		</div>
		<div class="text-center mt-4">
			<button class="btn btn-app disabled" id="save">Simpan</button>
		</div>
	</form>
	</div>
</div>
<script type="text/javascript">
	$('#formApp').on('click change keyup',function () {
		$('#pass,#repass').removeClass('form-error');
		$('#save').removeClass('disabled');
		if($('#pass').val() != $('#repass').val() || $('#pass').val().length <6 || $('#repass').val().length <6){
			$('#pass,#repass').addClass('form-error');
			$('#save').addClass('disabled');
		}
	})
</script>
@endsection