@extends('master.master_user')
@section('content')
<div class="col-app-user m-auto">
   @if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong>{{ $message }}</strong>
    </div>
  @endif
	<div class="m-auto text-bold p-4" style="max-width: 50rem">
	<form action="{{url('/user/update')}}" method="post" enctype="multipart/form-data">
		{{csrf_field()}}
		<div class="text-center">
			<img id="imgview" src="{{asset('images/user')}}/{{(Auth::user()->image)?Auth::user()->image:'profile.png'}}" class="image-view"/><br>
			<input name="image" type="file" accept="image/*" onchange="uploadimage(event)" id="imginput" class="hidden" />
			<label class="mt-2 text-center" for="imginput">
		      <span class="btn btn-app">Pilih Gambar</span>
		    </label>
		</div>
		<div class="row m-0 mt-3">
			<div class="col pl-0 font-18 pt-2 text-right" style="max-width: 120px">Nama</div>
			<div class="col pr-0"><input type="" name="name" class="form-control" value="{{Auth::user()->name}}"></div>
		</div>
		<div class="row m-0 mt-3">
			<div class="col pl-0 font-18 pt-2 text-right" style="max-width: 120px">Email</div>
			<div class="col pr-0 pt-2">{{Auth::user()->email}}</div>
		</div>
		<div class="row m-0 mt-3">
			<div class="col pl-0 font-18 pt-2 text-right" style="max-width: 120px">Password</div>
			<div class="col pr-0 pt-2">********* <a href="{{url('/user/change-password')}}">Ganti</a></div>
		</div>
		<div class="text-center mt-4">
			<button class="btn btn-app">Simpan</button>
		</div>
	</form>
	</div>
</div>
@endsection