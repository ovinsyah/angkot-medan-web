<nav id="sidebar" class="sidebar-mitra">
  <div class="sidebar-profile mt-2">
    <img src="{{asset('images/mitra')}}/{{Auth::user()->image}}" class="img-profile">
    <div class="sidebar-name-profile">
      <span>{{Auth::user()->nama}}</span>
    </div>
  </div>
  <ul class="list-unstyled components">
    <li class="{{ request()->is('mitra/index') ? 'active' : '' }}">
      <a href="{{url('/mitra/index')}}">
        <i class="glyphicon glyphicon-home"></i>
        Dashboard
      </a>
    </li>
    <li id="adm-trayek">
      <a id="adm-menu" href="#pageSubmenu" data-toggle="collapse" aria-expanded="false">
        <i class="glyphicon glyphicon-road"></i>
        Trayek
      </a>
      <ul class="collapse list-unstyled" id="pageSubmenu">
        <li id="adm-tr-angkot">
          <a href="{{url('/mitra/trayek-angkot')}}">Trayek Angkot</a></li>
        <li id="adm-tr-pending">
          <a href="{{url('/mitra/pending/trayek-angkot')}}">Trayek Pending</a></li>
      </ul>
    </li>
    <li id="adm-setting">
      <a id="adm-menu2" href="#subsetting" data-toggle="collapse" aria-expanded="false">
        <i class="glyphicon glyphicon-cog"></i>
        Pengaturan
      </a>
      <ul class="collapse list-unstyled" id="subsetting">
        <li id="adm-profile">
          <a href="{{url('/mitra/setting/profile')}}">Pengaturan Profile</a></li>
        <li id="adm-account">
          <a href="{{url('/mitra/setting/account')}}">Pengaturan Akun</a></li>
      </ul>
    </li>
    <li id="adm-report" class="{{ request()->is('mitra/report') ? 'active' : '' }}">
      <a href="{{url('/mitra/report')}}">
        <i class="glyphicon glyphicon-flag"></i>
        Laporan
      </a>
    </li>
  </ul>
</nav>