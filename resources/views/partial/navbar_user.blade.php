<nav class="navbar navbar-app">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="float-left navbar-toggle collapsed">
        <span class="icon-bar" style="background-color: #34495e "></span>
        <span class="icon-bar" style="background-color: #34495e "></span>
        <span class="icon-bar" style="background-color: #34495e "></span>
      </button>
      <a class="navbar-brand" href="{{url('/')}}">Home</a>
    </div>

    <div class="side-collapse collapse navbar-collapse" id="collapse-menu">
      <ul class="nav navbar-nav">
        <li class="{{ request()->is('angkot') ? 'active' : '' }}">
          <a href="{{url('/angkot')}}">Info Angkot</a>
        </li>
        <li class="{{ request()->is('contact') ? 'active' : '' }}">
          <a href="{{url('/contact')}}">Kontak</a>
        </li>

        </li>
        <li class="{{ request()->is('tampil') ? 'active' : '' }}">
          <a href="{{url('/tampil')}}">Test</a>
        </li>
      </ul>
      @guest
      <ul class="nav navbar-nav navbar-right">
        <li class="mr-2">
          <a href="{{url('/register')}}">Daftar</a>
        </li>
        <li class="mr-2">
          <a href="{{url('/login')}}">Login</a>
        </li>
        <li class="mr-2 hidden-sm-up">
          <a href="{{url('/join-mitra')}}">Daftar Mitra</a>
        </li>
        <li class="mr-2 hidden-sm-up">
          <a href="{{url('/mitra/login')}}">Login</a>
        </li>
      </ul>
      @else
      <ul class="nav navbar-nav navbar-right">
        <li>
          <div class="dropdown">
            <span class="pointer" data-toggle="dropdown">{{Auth::user()->name}}
              <span class="caret"></span></span>
              <ul class="dropdown-menu menu-user">
                <li><a href="{{url('/user')}}">Profile</a></li>
                <li><a href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">
                  Logout
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
                </form></li>
              </ul>
            </div>
          </li>
        </ul>
        @endguest
      </div>
    </div>
  </nav>
  <div class="back-drop hidden"></div>
  <script type="text/javascript">
    $('.collapsed,.back-drop').click(function () {
      $('.side-collapse').toggleClass('on');
      $('.back-drop').toggleClass('hidden');
    })
  </script>