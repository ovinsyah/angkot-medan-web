<nav class="navbar navbar-default m-0">
  <div class="container-fluid">
    <div class="navbar-header">
      <span class="font-18 text-bold mr-2">Angkot Medan</span>
      <button type="button" id="sidebarCollapse" class="btn btn-app bg-mitra rounded-0 navbar-btn">
        <i class="glyphicon glyphicon-fullscreen"></i>
      </button>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="{{ url('/mitra/logout') }}"> Logout</a>
        </li>
      </ul>
    </div>
  </div>
</nav>