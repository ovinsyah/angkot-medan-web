<nav id="sidebar">
  <div class="sidebar-header">
    <h3>Angkot Medan</h3>
    <strong>AM</strong>
  </div>
  <ul class="list-unstyled components">
    <li id="adm-dashboard" class="{{ request()->is('admin/index') ? 'active' : '' }}">
      <a href="{{url('/admin/index')}}">
        <i class="glyphicon glyphicon-home"></i>
        Dashboard
      </a>
    </li>
    <li id="adm-user" class="{{ request()->is('admin/member') ? 'active' : '' }}">
      <a href="{{url('/admin/member')}}">
        <i class="glyphicon glyphicon-user"></i>
        Member
      </a>
    </li>
    <li id="adm-mitra" class="{{ request()->is('admin/mitra') ? 'active' : '' }}">
      <a href="{{url('/admin/mitra')}}">
        <i class="glyphicon glyphicon-king"></i>
        Mitra
      </a>
    </li>
    <li id="adm-trayek">
      <a href="#pageSubmenu" id="adm-menu" data-toggle="collapse" aria-expanded="false">
        <i class="glyphicon glyphicon-road"></i>
        Trayek
      </a>
      <ul class="collapse list-unstyled" id="pageSubmenu">
        <li id="adm-tr-angkot"><a href="{{url('admin/trayek-angkot')}}">Trayek Angkot</a></li>
        <li id="adm-tr-pending"><a href="{{url('admin/pending/trayek-angkot')}}">Trayek Pending</a></li>
      </ul>
    </li>
    <li id="adm-contact" class="{{ request()->is('admin/contact') ? 'active' : '' }}">
      <a href="{{url('/admin/contact')}}">
        <i class="glyphicon glyphicon-send"></i>
        Contact
      </a>
    </li>
    <li id="adm-report" class="{{ request()->is('admin/report') ? 'active' : '' }}">
      <a href="{{url('/admin/report')}}">
        <i class="glyphicon glyphicon-flag"></i>
        Laporan
      </a>
    </li>
  <li id="adm-request" class="{{ request()->is('admin/request/mitra') ? 'active' : '' }}">
    <a href="{{url('/admin/request/mitra')}}">
      <i class="glyphicon glyphicon-tags"></i>
      Permohonan Mitra
    </a>
  </li>
  </ul>
</nav>