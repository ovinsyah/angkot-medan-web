<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" id="sidebarCollapse" class="btn btn-app rounded-0 navbar-btn">
        <i class="glyphicon glyphicon-fullscreen"></i>
      </button>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="{{ url('/admin/logout') }}"> Logout</a>

      </li>
      </ul>
    </div>
  </div>
</nav>