@extends('master.master_admin')
@section('content')
<div class="content-admin">
	<div class="ml-0 mr-0 mb-3 title-page-admin">
		<div>Detail Mitra {{$mitra->nama}}</div>
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Nama Perusahaan</span>
		</div>		
		<div class="col p-0">
			<span class="font-16 pt-2">{{$mitra->nama}}</span>
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Email Perusahaan</span>
		</div>		
		<div class="col p-0">
			<span class="font-16 pt-2">{{$mitra->email}}</span>
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Contact Perusahaan</span>
		</div>		
		<div class="col p-0">
			<span class="font-16 pt-2">{{$mitra->contact}}</span>
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Profile Perusahaan</span>
		</div>		
		<div class="col p-0">
		    <img src="{{asset('images/mitra')}}/{{$mitra->image}}" class="image-upload"/>
		</div>		
	</div>
	@if($mitra->bukti)
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Surat Pendukung</span>
		</div>		
		<div class="col p-0">
		    <img src="{{asset('images/mitra')}}/{{$mitra->bukti}}" class="image-upload"/>
		</div>		
	</div>
	@endif
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Alamat Perusahaan</span>
		</div>		
		<div class="col p-0">
			<span class="font-16 pt-2">{{$mitra->alamat}}</span>
		</div>		
	</div>
	<div class="row m-0 mb-5">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Deskripsi Perusahaan</span>
		</div>		
		<div class="col p-0">
			<span class="font-16 pt-2">{{$mitra->deskripsi}}</span>
		</div>		
	</div>
</div>
<script type="text/javascript">
	$('#adm-mitra').addClass('active');
</script>
@endsection