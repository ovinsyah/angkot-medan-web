@extends('master.master_admin')
@section('content')
<div class="content-admin">
	<div class="row ml-0 mr-0 mb-3 title-page-admin">
		<div class="col p-0">
			<div class="">Kontak</div>
		</div>
		<div class="col p-0 text-right">
			<button class="btn btn-app font-16" data-toggle="modal" data-target="#modalAdd">
				<span class="glyphicon glyphicon-plus mr-2"></span>Kontak
			</button>
		</div>
	</div>
	<table id="table_id" class="display">
		<thead>
			<tr>
				<th>Judul</th>
				<th>Icon</th>
				<th>Deskripsi</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			@foreach($contacts as $contact)
			<tr>
				<td>{{$contact->judul}}</td>
				<td>{{$contact->icon}}</td>
				<td>{{$contact->deskripsi}}</td>
				<td>
					<a href="" class="btn btn-app-icon-1" data-toggle="modal" data-target="#modalEdit">
						<span class="glyphicon glyphicon-pencil"></span>
					</a>
					<a href="" class="btn btn-app-icon-2" data-toggle="modal" data-target="#modalRemove{{$contact->id}}">
						<span class="glyphicon glyphicon-trash"></span>
					</a>
				</td>
			</tr>
			<!-- Modal -->
			<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">Tambah Kontak</h4>
						</div>
						<div class="modal-body">
							<form method="post" action="{{url('admin/update/contact',$contact->id)}}" enctype="multipart/form-data">
								{{csrf_field()}}
							<div class="row m-0 mb-3">
								<div class="col p-0" style="max-width: 13rem">
									<span class="text-bold  font-16 pt-2">Judul</span>
								</div>
								<div class="col p-0">
									<input type="" name="judul" class="form-control" value="{{$contact->judul}}">
								</div>
							</div>
							<div class="row m-0 mb-3">
								<div class="col p-0" style="max-width: 13rem">
									<span class="text-bold  font-16 pt-2">Deskripsi</span>
								</div>
								<div class="col p-0">
									<textarea class="form-control" rows="4" name="deskripsi">{{$contact->deskripsi}}</textarea>
								</div>
							</div>
							<div class="row m-0 mb-3">
								<div class="col p-0" style="max-width: 13rem">
									<span class="text-bold  font-16 pt-2">Icon</span>
								</div>
								<div class="col p-0">
									<div class="image-view-upload d-inline-block">
										<input name="icon" type="file" accept="image/*" onchange="uploadimage()" id="imginput" class="hidden" />
										<img id="imgview" src="{{asset('images/contact')}}/{{$contact->icon}}" class="image-upload"/><br>
										<label class="mt-2 btn-change text-center" for="imginput">
											<span class="btn btn-upload">Pilih Gambar</span>
										</label>
									</div>
								</div>
							</div>

						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
							<button type="submit" class="btn btn-primary">Ya</button>
						</div>
						</form>
					</div>
				</div>
			</div>

			<!-- Modal delete-->
<div class="modal fade" id="modalRemove{{$contact->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Hapus Kontak</h4>
			</div>
			<div class="modal-body">
				Apakah anda yakin menghapus kontak ini ?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
				<a href="{{url('admin/delete/contact',$contact->id)}}" class="btn btn-primary">Ya</a>
			</div>
		</div>
	</div>
</div>
			@endforeach
		</tbody>
	</table>
</div>
<!-- Modal -->
<div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Tambah Kontak</h4>
			</div>
			<div class="modal-body">
				<form method="post" action="{{url('admin/create/contact')}}" enctype="multipart/form-data">
					{{csrf_field()}}
				<div class="row m-0 mb-3">
					<div class="col p-0" style="max-width: 13rem">
						<span class="text-bold  font-16 pt-2">Judul</span>
					</div>
					<div class="col p-0">
						<input type="" name="judul" class="form-control">
					</div>
				</div>
				<div class="row m-0 mb-3">
					<div class="col p-0" style="max-width: 13rem">
						<span class="text-bold  font-16 pt-2">Deskripsi</span>
					</div>
					<div class="col p-0">
						<textarea class="form-control" rows="4" name="deskripsi"></textarea>
					</div>
				</div>
				<div class="row m-0 mb-3">
					<div class="col p-0" style="max-width: 13rem">
						<span class="text-bold  font-16 pt-2">Icon</span>
					</div>
					<div class="col p-0">
						<div class="image-view-upload d-inline-block">
							<input name="icon" type="file" accept="image/*" onchange="uploadimage()" id="imginput" class="hidden" />
							<img id="imgview" src="{{asset('img/angkot.jpg')}}" class="image-upload"/><br>
							<label class="mt-2 btn-change text-center" for="imginput">
								<span class="btn btn-upload">Pilih Gambar</span>
							</label>
						</div>
					</div>
				</div>

			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
				<button type="submit" class="btn btn-primary">Ya</button>
			</div>
			</form>
		</div>
	</div>
</div>


<script type="text/javascript">
	$(document).ready( function () {
		$('#table_id').DataTable();
	} );
</script>
@endsection