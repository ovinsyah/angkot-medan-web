@extends('master.master_admin')
@section('content')
<div class="content-admin">
	<div class="ml-0 mr-0 mb-3 title-page-admin">
		<div>Terima Mitra</div>
	</div>
	<form method="post" action="{{url('admin/update/mitra',$mitra->id)}}" enctype="multipart/form-data">
		{{csrf_field()}}
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Nama Perusahaan</span>
		</div>		
		<div class="col p-0">
			<input type="" name="nama" class="form-control" style="width: 30rem" value="{{$mitra->nama}}">
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Email Perusahaan</span>
		</div>		
		<div class="col p-0">
			<input type="email" name="email" class="form-control" style="width: 30rem" value="{{$mitra->email}}">
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Contact Perusahaan</span>
		</div>		
		<div class="col p-0">
			<input type="telp" name="contact" class="form-control" style="width: 30rem" value="{{$mitra->contact}}">
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Password</span>
		</div>		
		<div class="col p-0">
			<input type="password" name="password" class="form-control password" style="width: 30rem" id="pass">
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Konfirmasi Password</span>
		</div>		
		<div class="col p-0">
			<input type="password" name="" class="form-control password" style="width: 30rem" id="repass">
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Profile Perusahaan</span>
		</div>		
		<div class="col p-0">
			<div class="image-view-upload d-inline-block">
			<input name="image" type="file" accept="image/*" onchange="uploadimage()" id="imginput" class="hidden" />
		    <img id="imgview" src="{{asset('images/mitra')}}/{{$mitra->image}}" class="image-upload"/><br>
		    <label class="mt-2 btn-change text-center" for="imginput">
		      <span class="btn btn-upload">Pilih Gambar</span>
		    </label>
			</div>
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Alamat Perusahaan</span>
		</div>		
		<div class="col p-0">
			<textarea class="form-control" rows="6"name="alamat" >{{$mitra->alamat}}</textarea>
		</div>		
	</div>
	<div class="row m-0 mb-5">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Deskripsi Perusahaan</span>
		</div>		
		<div class="col p-0">
			<textarea class="form-control" rows="6" name="deskripsi">{{$mitra->deskripsi}}</textarea>
		</div>		
	</div>
	<div class="text-right">
		<button class="btn btn-app" type="submit" id="confirm">Simpan</button>
	</div>
</form>
</div>
<script type="text/javascript">
	$('#adm-request').addClass('active');

	$('.password').on('click change keyup',function () {
		if($('#pass').val() != $('#repass').val()){
			$('#pass,#repass').addClass('form-error');
			$('#confirm').addClass('disabled');
		}
		else{
			$('#pass,#repass').removeClass('form-error');
			$('#confirm').removeClass('disabled');
		}
	});
</script>
@endsection