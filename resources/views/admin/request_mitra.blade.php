@extends('master.master_admin')
@section('content')
<div class="content-admin">
	<div class="ml-0 mr-0 mb-3 title-page-admin">
		<div class="">Permohonan Mitra</div>
	</div>
	<table id="table_id" class="display">
		<thead>
			<tr>
				<th>Nama Perusahaan</th>
				<th>Email</th>
				<th>Contact</th>
				<th>Alamat</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			@foreach($mitras as $mitra)
			<tr>
				<td><a href="{{url('admin/detail/mitra',$mitra->id)}}">{{$mitra->nama}}</a></td>
				<td><a href="{{url('admin/detail/mitra',$mitra->id)}}">{{$mitra->email}}</a></td>
				<td><a href="{{url('admin/detail/mitra',$mitra->id)}}">{{$mitra->contact}}</a></td>
				<td><a href="{{url('admin/detail/mitra',$mitra->id)}}">{{$mitra->alamat}}</a></td>
				<td>
					<a href="{{url('admin/detail/mitra',$mitra->id)}}" class="btn btn-app-icon-1">
						<span class="glyphicon glyphicon-eye-open"></span>
					</a>
					<a href="{{url('/admin/accept/request/mitra',$mitra->id)}}" class="btn btn-app-icon-3">
						<span class="glyphicon glyphicon-ok"></span>
					</a>
					<a href="" dataid="{{$mitra->id}}" dataname="{{$mitra->name}}" class="btn btn-app-icon-2 delete" data-toggle="modal" data-target="#modalRemove">
						<span class="glyphicon glyphicon-trash"></span>
					</a>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
<!-- Modal delete-->
<div class="modal fade" id="modalRemove" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Hapus Permohonan Mitra</h4>
			</div>
			<div class="modal-body">
				Apakah anda yakin menghapus permohonan mitra <span id="name_mitra"></span> ini ?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
				<button id="confirm" href="" class="btn btn-primary">Ya</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready( function () {
		$('#table_id').DataTable();

		var id;
		$('.delete').click(function () {
			id = $(this).attr('dataid');
			var name = $(this).attr('datanamevar');
			$('#name_mitra').text(name);
		});
		$('#confirm').click(function () {
			$.get("/admin/delete/mitra/"+id, function(data, status){
				console.log('terhapus '+id);
				location.href="/admin/request/mitra";
			});
		})
	});
</script>
@endsection