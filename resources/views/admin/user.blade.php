@extends('master.master_admin')
@section('content')
<div class="content-admin">
	<div class="title-page-admin mb-3">Member</div>
	<table id="table_id" class="display">
		<thead>
			<tr>
				<th>Nama member</th>
				<th>Email</th>
				<th>Tanggal</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			@foreach($members as $member)
			<tr>
				<td>{{$member->name}}</td>
				<td>{{$member->email}}</td>
				<td>{{$member->created_at}}</td>
				<td>
					<a href="" class="btn btn-app-icon-2" data-toggle="modal" data-target="#modalRemove{{$member->id}}">
						<span class="glyphicon glyphicon-trash"></span>
					</a>
				</td>
			</tr>
									<!-- Modal -->
			<div class="modal fade" id="modalRemove{{$member->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="myModalLabel">Hapus Member</h4>
			      </div>
			      <div class="modal-body">
			        Apakah anda yakin menghapus member {{$member->name}} ?
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
			        <a href="{{url('admin/delete/member',$member->id)}}" class="btn btn-primary">Ya</a>
			      </div>
			    </div>
			  </div>
			</div>
			@endforeach
		</tbody>
	</table>
</div>
<script type="text/javascript">
	$('#table_id').DataTable();
</script>
@endsection