@extends('master.master_admin')
@section('content')
<div class="content-admin">
	<div class="ml-0 mr-0 mb-3 title-page-admin">
		<div>Tambah Mitra</div>
	</div>
	<form method="post" action="{{url('admin/create/mitra')}}" enctype="multipart/form-data" id="form">
		{{csrf_field()}}
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Nama Perusahaan</span>
		</div>		
		<div class="col p-0">
			<input type="" name="nama" class="form-control" style="width: 30rem" required>
		</div>		
	</div>
	<div class="row m-0 mb-3 {{ $errors->has('email') ? ' has-error' : '' }}">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Email Perusahaan</span>
		</div>		
		<div class="col p-0">
			<input type="email" name="email" class="form-control" style="width: 30rem" required value="{{ old('email') }}">
		</div>
		@if ($errors->has('email'))
			    <span class="help-block">
			    	<strong>Email Telah Digunakan</strong>
			    </span>
		@endif			
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Contact Perusahaan</span>
		</div>		
		<div class="col p-0">
			<input type="telp" name="contact" class="form-control" style="width: 30rem" required>
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Password</span>
		</div>		
		<div class="col p-0">
			<input type="password" name="password" class="form-control password" style="width: 30rem" id="pass" required>
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Konfirmasi Password</span>
		</div>		
		<div class="col p-0">
			<input type="password" name="" class="form-control password" style="width: 30rem" id="repass" required>
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Profile Perusahaan</span>
		</div>		
		<div class="col p-0">
			<div class="image-view-upload d-inline-block">
			<input name="image" type="file" accept="image/*" onchange="uploadimage(event)" id="imginput" class="hidden" />
		    <img id="imgview" src="{{asset('img/angkot.jpg')}}" class="image-upload"/><br>
		    <label class="mt-2 btn-change text-center" for="imginput">
		      <span class="btn btn-upload">Pilih Gambar</span>
		    </label>
			</div>
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Alamat Perusahaan</span>
		</div>		
		<div class="col p-0">
			<textarea class="form-control" rows="6" name="alamat" required ></textarea>
		</div>		
	</div>
	<div class="row m-0 mb-5">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Deskripsi Perusahaan</span>
		</div>		
		<div class="col p-0">
			<textarea class="form-control" rows="6" name="deskripsi" required></textarea>
		</div>		
	</div>
	<div class="text-right">
		<button class="btn btn-app disabled" type="submit" id="confirm">Simpan</button>
	</div>
	</form>
</div>
<script type="text/javascript">
	$('#adm-mitra').addClass('active');

	var status = 1;
	$('#form').on('click change keyup',function () {
		status = 1;
		if($('#pass').val() != $('#repass').val()){
			$('#pass,#repass').addClass('form-error');
			status = 0;
		}
		else{
			$('#pass,#repass').removeClass('form-error');
		}
		if($('#imginput').val() == ''){
			$('#imgview').addClass('form-error')
			status = 0;
		}
		else{
			$('#imgview').removeClass('form-error');
		}
		if(status == 1){
			$('#confirm').removeClass('disabled');
		}
		else{
			$('#confirm').addClass('disabled');
		}
	});
</script>
@endsection