@extends('master.master_admin')
@section('content')
<div class="content-admin">
	<div class="title-page-admin mb-5">Dashboard</div>
	<div class="row m-0">
		<div class="col-4 mb-5">
			<a href="{{url('admin/mitra')}}">
				<div class="row m-0 col-dash-mitra">
					<div class="col p-2">
						<img src="{{asset('img/angkot.png')}}" style="width: 80%">
					</div>
					<div class="col text-center pt-4">
						<h1 class="m-0 text-bold">{{count(App\Mitra::where('status',1)->get())}}</h1>
						<span class="font-16 mt-2 text-bold">Mitra</span>
					</div>
				</div>
			</a>
		</div>
		<div class="col-4 mb-5">
			<a href="{{url('admin/request/mitra')}}">
				<div class="row m-0 col-dash-mitra">
					<div class="col p-2">
						<img src="{{asset('img/angkot_pending.png')}}" style="width: 80%">
					</div>
					<div class="col text-center pt-4">
						<h1 class="m-0 text-bold">{{count(App\Mitra::where('status',0)->get())}}</h1>
						<span class="font-16 mt-2 text-bold">Permohonan Mitra</span>
					</div>
				</div>
			</a>
		</div>
		<div class="col-4 mb-5">
			<a href="{{url('admin/report')}}">
				<div class="row m-0 col-dash-mitra">
					<div class="col p-2">
						<img src="{{asset('img/report.png')}}" style="width: 80%">
					</div>
					<div class="col text-center pt-4">
						<h1 class="m-0 text-bold">{{count(App\Laporan::get())}}</h1>
						<span class="font-16 mt-2 text-bold">Laporan Pengguna</span>
					</div>
				</div>
			</a>
		</div>
		<div class="col-4 mb-5">
			<a href="{{url('admin/trayek-angkot')}}">
				<div class="row m-0 col-dash-mitra">
					<div class="col p-2">
						<img src="{{asset('img/rute.png')}}" style="width: 80%">
					</div>
					<div class="col text-center pt-4">
						<h1 class="m-0 text-bold">{{count(App\Angkot::where('status',1)->get())}}</h1>
						<span class="font-16 mt-2 text-bold">Trayek Angkot Disetujui</span>
					</div>
				</div>
			</a>
		</div>
		<div class="col-4 mb-5">
			<a href="{{url('admin/pending/trayek-angkot')}}">
				<div class="row m-0 col-dash-mitra">
					<div class="col p-2">
						<img src="{{asset('img/rute-pending.png')}}" style="width: 80%">
					</div>
					<div class="col text-center pt-4">
						<h1 class="m-0 text-bold">{{count(App\Angkot::where('status',0)->get())}}</h1>
						<span class="font-16 mt-2 text-bold">Trayek Angkot Pending</span>
					</div>
				</div>
			</a>
		</div>
	</div>
</div>
@endsection