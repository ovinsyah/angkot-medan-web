@extends('master.master_admin')
@section('content')
<div class="content-admin">
	<div class="row ml-0 mr-0 mb-3 title-page-admin">
		<div class="col p-0">
			<div class="">Mitra</div>
		</div>
		<div class="col p-0 text-right">
		<a href="{{url('admin/add/mitra')}}" class="btn btn-app font-16">
				<span class="glyphicon glyphicon-plus mr-2"></span>Mitra
			</a>
		</div>
	</div>
	<table id="table_id" class="display">
		<thead>
			<tr>
				<th>Nama Perusahaan</th>
				<th>Email</th>
				<th>Contact</th>
				<th>Alamat</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			@foreach($mitras as $mitra)
			<tr>
				<td><a href="{{url('/admin/detail/mitra',$mitra->id)}}">{{$mitra->nama}}</a></td>
				<td><a href="{{url('/admin/detail/mitra',$mitra->id)}}">{{$mitra->email}}</a></td>
				<td><a href="{{url('/admin/detail/mitra',$mitra->id)}}">{{$mitra->contact}}</a></td>
				<td><a href="{{url('/admin/detail/mitra',$mitra->id)}}">{{$mitra->alamat}}</a></td>
				<td>
					<a href="{{url('/admin/detail/mitra',$mitra->id)}}" class="btn btn-app-icon-1">
						<span class="glyphicon glyphicon-eye-open"></span>
					</a>
					<a data2="{{$mitra->nama}}" data="{{$mitra->id}}" href="#" class="btn btn-app-icon-2 delete" data-toggle="modal" data-target="#modalRemove">
						<span class="glyphicon glyphicon-trash"></span>
					</a>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
<!-- Modal -->
<div class="modal fade" id="modalRemove" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Hapus Mitra</h4>
      </div>
      <div class="modal-body">
        Apakah anda yakin menghapus <span id="name_mitra"></span> ?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" id="confirm">Ya</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	$(document).ready( function () {
    $('#table_id').DataTable();

    var id;
    $('.delete').click(function () {
    	id = $(this).attr('data');
    	var nama = $(this).attr('data2');
    	$('#name_mitra').text(nama);
    })
    $('#confirm').click(function () {
    $.get("/admin/delete/mitra/"+id, function(data, status){
        console.log('terhapus '+id);
        location.href="/admin/mitra";
    });
    })
} );
</script>
@endsection