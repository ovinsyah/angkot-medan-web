@extends('master.master_admin')
@section('content')
<div class="content-admin">
	<div class="title-page-admin mb-3">Detail Laporan</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Nama Pengguna</span>
		</div>		
		<div class="col p-0">
			<span class="font-16 pt-2">{{$laporan->user->name}}</span>
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Nomor Angkot</span>
		</div>		
		<div class="col p-0">
			<span class="font-16 pt-2">{{$laporan->angkot->nomor}}</span>
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Waktu Laporan</span>
		</div>		
		<div class="col p-0">
			<span class="font-16 pt-2">{{$laporan->tanggal}}</span>
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Nomor polisi</span>
		</div>		
		<div class="col p-0">
			<span class="font-16 pt-2">{{$laporan->nopol}}</span>
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Nama Perusahaan</span>
		</div>		
		<div class="col p-0">
			<span class="font-16 pt-2">{{$laporan->mitra->nama}}</span>
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Isi Laporan</span>
		</div>		
		<div class="col p-0">
			<span class="font-16 pt-2">
				{{$laporan->isi}}
			</span>
		</div>		
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 200px">
			<span class="text-bold font-16 pt-2">Pembelaan Mitra</span>
		</div>		
		<div class="col p-0">
			<span class="font-16">
				@if($laporan->balasan == '')
					<div class="bg-danger p-1">Tidak Ada</div>
				@else
				{{$laporan->balasan}}
				@endif
			</span>
		</div>		
	</div>
</div>
<script type="text/javascript">
	$('#adm-report').addClass('active');
</script>
@endsection