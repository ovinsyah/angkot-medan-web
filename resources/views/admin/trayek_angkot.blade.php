@extends('master.master_admin')
@section('content')
<div class="content-admin">
	<div class="ml-0 mr-0 mb-3 title-page-admin">
			<div>Trayek Angkot</div>
	</div>
	<table id="table_id" class="display">
		<thead>
			<tr>
				<th>Nomor Angkot</th>
				<th>Rute</th>
				<th>Perusahaan</th>
				<th width="100">Aksi</th>
			</tr>
		</thead>
		<tbody>
			@foreach($angkots as $angkot)
			<tr>
				<td><a href="{{url('admin/detail/trayek-angkot',$angkot->id)}}">{{$angkot->nomor}}</a></td>
				<td><a class="ellipsis-3" href="{{url('admin/detail/trayek-angkot',$angkot->id)}}">{{$angkot->rutes}}</a></td>
				<td><a href="{{url('admin/detail/trayek-angkot',$angkot->id)}}">{{$angkot->mitra->nama}}</a></td>
				<td>
					<a href="{{url('admin/detail/trayek-angkot',$angkot->id)}}" class="btn btn-app-icon-1">
						<span class="glyphicon glyphicon-eye-open"></span>
					</a>
					<a href="" class="btn btn-app-icon-2" data-toggle="modal" data-target="#modalRemove{{$angkot->id}}">
						<span class="glyphicon glyphicon-trash"></span>
					</a>
				</td>
			</tr>
			<!-- Modal -->
			<div class="modal fade" id="modalRemove{{$angkot->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="myModalLabel">Hapus angkot</h4>
			      </div>
			      <div class="modal-body">
			        Apakah anda yakin menghapus trayek {{$angkot->nomor}} ?
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
			        <a href="{{url('admin/delete/trayek',$angkot->id)}}" class="btn btn-primary">Ya</a>
			      </div>
			    </div>
			  </div>
			</div>
			@endforeach
		</tbody>
	</table>
</div>

<script type="text/javascript">
	$(document).ready( function () {
    $('#table_id').DataTable();

	$('#adm-trayek').addClass('active');
	$('#adm-trayek').removeClass('collapsed');
    $('#pageSubmenu').attr( "aria-expanded", "true" );
	$('#pageSubmenu').addClass('in');
	$('#pageSubmenu').addClass('show');
	$('#adm-menu').attr( "aria-expanded", "true" );
	$('#adm-tr-angkot').addClass('active');

} );
</script>
@endsection