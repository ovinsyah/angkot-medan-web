<?php
use App\Angkot;
use Illuminate\Support\Facades\Mail;
use App\Mail\AdminMail;
Route::get('/uniq', function () {
	$rutes=Angkot::where('status','!=',0)->get();
	$all='';
	foreach ($rutes as $key) {
			$all.=$key->rutes.',';
	}
		$all=explode(",",$all);
		$uniqs=explode(",",implode(',',array_unique($all)));
		foreach ($uniqs as $key ) {
			if (strpos($key, 'Jalan') === false) {
				$uniq[]=$key;
			}
		}
		
    return $uniq;
});

Route::get('/test', function () {
	return str_replace("worl","Peter","Hello world!");
});


//Auth aktor
Auth::routes();
Route::get('/tampil','CobaController@test');
Route::get('user/logout','Auth\LoginController@logout_admin');
Route::get('mitra/login','AuthMitra\LoginController@showLoginForm');
Route::post('mitra/login','AuthMitra\LoginController@login');
Route::get('mitra/logout','AuthMitra\LoginController@logout_mitra');

Route::get('admin/register','AuthAdmin\RegisterController@showRegistrationForm');
Route::post('admin/register','AuthAdmin\RegisterController@register');
Route::get('admin/login','AuthAdmin\LoginController@showLoginForm');
Route::post('admin/login','AuthAdmin\LoginController@login');
Route::get('admin/logout','AuthAdmin\LoginController@logout_admin');

Route::get('/email/{id}', function ($id) {
	 $objDemo = new \stdClass();
        $objDemo->usaha = 'usaha';
        $objDemo->email = 'email weru';
        $objDemo->password = '123456';
 
        Mail::to($id)->send(new AdminMail($objDemo));
        return "berhasil";
});
Route::get('admin', function () {
    return redirect('admin/index');
});
Route::get('mitra', function () {
    return redirect('mitra/index');
});

Route::get('tst', function () {
    return redirect('/join-mitra')->with('success','Saved succesfully!');
});
Route::get('/send-mail','UserIndexController@sendmail');

Route::get('/','UserIndexController@index');
Route::get('/search-trayek/{awal}/{tujuan}','UserIndexController@search');
Route::get('/detail/trayek/{id}','UserIndexController@detail');
Route::get('/report/trayek/{id}','UserIndexController@report');
Route::get('/api/detail/trayek/{id}','UserIndexController@api_detail');

Route::get('/angkot','UserIndexController@angkot');
Route::get('/search-angkot','UserIndexController@searchangkot');

Route::get('/contact','UserIndexController@contact');
Route::get('/join-mitra','UserIndexController@mitra');
Route::post('/join-mitra','UserIndexController@create');

//crud
Route::post('/report/trayek/{id}','UserIndexController@create_report');


/*#ADMIN
=======================================================*/
Route::prefix('admin')->middleware('auth:admin')->group(function () {
	Route::get('index','AdminIndexController@index');
	Route::get('member','AdminIndexController@member');
	Route::get('contact','AdminIndexController@contact');
	Route::get('report','AdminIndexController@report');
	Route::get('detail/report/{id}','AdminIndexController@detailreport');
	Route::get('delete/report/{id}','AdminIndexController@deletereport');


	Route::get('mitra','AdminMitraController@index');
	Route::get('add/mitra','AdminMitraController@add');
	Route::get('detail/mitra/{id}','AdminMitraController@detail');
	Route::get('request/detail/mitra','AdminMitraController@requestdetail');

	Route::get('request/mitra','AdminMitraController@request');
	Route::get('accept/request/mitra/{id}','AdminMitraController@acceptrequest');

	//crud mitra
	Route::post('create/mitra','AdminMitraController@create');
	Route::post('update/mitra/{id}','AdminMitraController@update');
	Route::get('delete/mitra/{id}','AdminMitraController@delete');

	//crud contact
	Route::post('create/contact','AdminIndexController@create_contact');
	Route::post('update/contact/{id}','AdminIndexController@update_contact');
	Route::get('delete/contact/{id}','AdminIndexController@delete_contact');

	//crud trayek
	Route::get('delete/trayek/{id}','AdminTrayekController@delete_trayek');
	Route::get('approve/trayek/{id}','AdminTrayekController@approve_trayek');

	//crud member
	Route::get('delete/member/{id}','AdminIndexController@delete_member');

	Route::get('/trayek-angkot','AdminTrayekController@index');
	Route::get('/detail/trayek-angkot/{id}','AdminTrayekController@detail');
	Route::get('/pending/trayek-angkot','AdminTrayekController@pending');
	Route::get('/pending/detail/trayek-angkot/','AdminTrayekController@pendingdetail');
});
/*#MITRA
=======================================================*/
Route::group(['prefix'=>'mitra','middleware'=>'auth:mitra'],function () {
	Route::get('index','MitraIndexController@index');
	Route::get('report','MitraIndexController@report');
	Route::get('detail/report/{id}','MitraIndexController@detailreport');

	Route::get('trayek-angkot','MitraTrayekController@index');
	Route::get('add/trayek-angkot','MitraTrayekController@add');
	Route::get('edit/trayek-angkot/{id}','MitraTrayekController@edit');

	Route::get('/pending/trayek-angkot','MitraTrayekController@pending');
	Route::get('/pending/edit/trayek-angkot','MitraTrayekController@editpending');

	Route::get('/detail/trayek-angkot/{id}','MitraTrayekController@detail');
	Route::get('/pending/detail/trayek-angkot','MitraTrayekController@pendingdetail');

	//crud trayek
	Route::post('create/trayek','MitraTrayekController@create_trayek');
	Route::post('update/trayek/{id}','MitraTrayekController@update_trayek');
	Route::get('delete/trayek/{id}','MitraTrayekController@delete_trayek');

	//crud trayek pending
	Route::get('delete/trayek/pending/{id}','MitraTrayekController@delete_trayek_pending');

	Route::get('setting/account','MitraSettingController@account');
	Route::get('setting/change-password','MitraSettingController@changepassword');
	Route::get('setting/profile','MitraSettingController@profile');
	Route::get('setting/edit/profile','MitraSettingController@editprofile');

	Route::post('setting/update/profile','MitraSettingController@updateprofile');
	Route::post('setting/update/password','MitraSettingController@updatepassword');

	Route::post('reply/report/{id}','MitraSettingController@replayreport');
});
/*#User
=======================================================*/
Route::group(['prefix'=>'user','middleware'=>'auth:web'],function () {
	Route::get('/','MemberController@index');
	Route::post('/update','MemberController@update_profile');
	Route::post('/update/password/user','MemberController@update_password');
	Route::get('/edit','MemberController@edit');
	Route::get('/change-password','MemberController@password');

});


